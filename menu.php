<?php 
if ($pageheading=='Course') //level=2
{
	$path='../../';
}
else //About FES, Academcis, Faculty, FES Coucil, Industry and Careers, Join FES, Labs, Research, Students
{
	$path='../';
}

echo 
'<ul>
    <li class="first">';
	
     if($menu==1)
		{ echo"<a class='selected' href='".$path."About FES/Overview.php'>About FES</a>";}
		else
		{ echo"<a class='hide' href='".$path."About FES/Overview.php'>About FES</a>";}
        	
            echo "<ul>
            	<li><a href='".$path."About FES/Overview.php'>Overview</a></li>
           		<li><a href='".$path."About FES/MissionAndObjectives.php'>Mission and Objectives</a></li>
           		<li><a href='".$path."About FES/DeansMessage.php'>Dean's Message</a></li>
                <li><a href='".$path."About FES/WhatIsEngSci.php'>What is Engineering Sciences?</a></li>
        	</ul>
	</li>
    
    <li>";
   
    if($menu==2)
		{ echo"<a class='selected' href='#'>Join FES</a>";}
		else
		{ echo"<a class='hide' href='#'>Join FES</a>";}
           echo' <ul>
                <li><a href="#">Undergraduate Student</a></li>
                <li><a href="#">Graduate Student</a></li>
                <li><a href="#">Faculty Member</a></li>
            </ul>         
    </li>
    
    <li>';
    
	if($menu==3)
		{ echo"<a class='selected' href='".$path."Academics/EngSciProgram.php'>Academics</a>";}
		else
		{ echo"<a class='hide' href='".$path."Academics/EngSciProgram.php'>Academics</a>";}
            echo '<ul>
                <li><a href="'.$path.'Academics/EngSciProgram.php">Engineering Sciences Program</a></li>
                <li><a href="'.$path.'Academics/DegreePlan.php">Degree Plan</a></li>
				<li><a href="'.$path.'Academics/Courses.php">Courses</a></li>
                <li><a href="'.$path.'Academics/Specializations.php">Specializations</a></li>
                
            </ul>         
    </li>
    
    <li>';
   
   	if($menu==4)
		{ echo"<a class='selected' href='".$path."Laboratories/TeachingLabs.php'>Laboratories</a>";}
		else
		{ echo"<a class='hide' href='".$path."Laboratories/TeachingLabs.php'>Laboratories</a>";}
			echo '<ul>
                <li><a href="'.$path.'Laboratories/TeachingLabs.php">Teaching Laboratories</a></li>
                <li><a href="'.$path.'Laboratories/LabManuals.php">Student Lab Manuals/Softwares</a></li>
            </ul> 
   
    </li>
    
    <li>';
    
	if($menu==5)
		{ echo"<a class='selected' href='".$path."Research/ResearchAreas.php'>Research</a>";}
		else
		{ echo"<a class='hide' href='".$path."Research/ResearchAreas.php'>Research</a>";}
            echo '<ul>
                <li><a href="'.$path.'Research/ResearchAreas.php">Research Areas</a></li>
                <li><a href="'.$path.'Research/ResearchLabs.php">Research Labs</a></li>
            </ul>         
    </li>
    
    <li>';
    
	if($menu==6)
		{ echo"<a class='selected' href='".$path."FES Council/Overview.php'>FES Council</a>";}
		else
		{ echo"<a class='hide' href='".$path."FES Council/Overview.php'>FES Council</a>";}
            echo '<ul>
                <li><a href="'.$path.'FES Council/Overview.php">Overview</a></li>
                <li><a href="'.$path.'FES Council/ExecutiveCouncil.php">Executive Council</a></li>
                <li><a href="'.$path.'FES Council/Membership.php">Membership</a></li>
            </ul>         
    </li>
    
    <li class="last">';
    
	if($menu==7)
		{ echo"<a class='selected' href='http://www.giki.edu.pk/AboutGIKI/Overview'>About GIKI</a>";}
		else
		{ echo"<a class='hide' href='http://www.giki.edu.pk/AboutGIKI/Overview'>About GIKI</a>";}
            echo '<ul>
                <li><a href="http://www.giki.edu.pk/AboutGIKI/Overview">Overview</a></li>
                <li><a href="http://www.giki.edu.pk/AboutGIKI/Genesis">Genesis</a></li>
                <li><a href="http://www.giki.edu.pk/AboutGIKI/Vision">Vision</a></li>
            </ul>         
    </li>
    
</ul>';?>