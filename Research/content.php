<?php echo     	'';
     switch($pageheading){
	case 'Research Areas':
	echo"
          	<h1>Research Areas</h1>
			
			<h2>Computational Physics</h2>
			<p>Computational physics is the study and implementation of numerical algorithms to solve problems in
			physics for which a quantitative theory already exists. It involves the uses modern computer simulation
			techniques combined with analytical theory to study a broad range of condensed matter systems with
			an emphasis on the statistical mechanics of equilibrium and non-equilibrium processes.</p>
			<p><b>Involved Faculty Member(s):</b>Dr. Jamil-un-nabi</p>
			
			<h2>Nuclear Physics</h2>
			<p>Nuclear physics is the field of physics that studies the constituents and interactions of atomic nuclei.
			research in nuclear physics has provided application in many fields, including those in nuclear medicine
			and magnetic resonance imaging, ion implantation in materials engineering, and radiocarbon dating in
			geology and archaeology.</p>
			<p><b>Involved Faculty Member(s):</b> Prof. Dr. Bakhadir F. Irgaziev</p>
			
			<h2>General Relativity</h2>
			<p>General relativity, or the general theory of relativity, is the geometric theory of gravitation published
			by Albert Einstein in 1916, and the current description of gravitation in modern physics. General
			relativity generalises special relativity and Newton's law of universal gravitation, providing a unified
			description of gravity as a geometric property of space and time, or spacetime.</p>
			<p><b>Involved Faculty Member(s):</b> Prof. Dr. Ghulam Shabbir</p>
			
			<h2>Analysis of Non-Local Lattice Models</h2>
			<p>Analysis of Non-Local Lattice Models and Applications in Dislocation Theory.</p>
			<p>A dislocation is a crystallographic
			defect, or irregularity, within a crystal structure. The presence of dislocations strongly influences many
			of the properties of materials, which can be used to our advantage.</p>
			<p><b>Involved Faculty Member(s):</b> Dr. Sirajul Haq</p>
			
			<h2>Spectroscopy</h2>
			<p>Spectroscopy is the study of the interaction between matter and radiated energy. Spectroscopic studies
			were central to the development of quantum mechanics, the explanation of the photoelectric effect
			and the explanation of atomic structure and spectra. Spectroscopy is used in physical and analytical
			chemistry, astronomy, remote sensing, etc.</p>
			<p><b>Involved Faculty Member(s):</b> Prof. Dr. Muhammad Hassan Sayyad</p>
			
			<h2>Magnetic Thin Films</h2>
			<p>The field of Magnetic Thin Films involves the study of the coupling between ferromagnetic (FM) layers
			separated by a non-ferromagentic or an antiferromagnetic (AFM) spacer. These systems provide new
			standards for magnetic materials and devices used in magnetic data storage, magnetic sensors and
			biomedical industries.</p>
			<p><b>Involved Faculty Member(s):</b> Engr. Dr. Rizwan Ahmad Khan, Engr. Saleem K han</p>
			
			<h2>Numerical Analysis</h2>
			<p>Numerical analysis naturally finds applications in all fields of engineering and the physical sciences. It is
			the study of algorithms that use numerical approximation for the problems of mathematical analysis.</p>
			<p><b>Involved Faculty Member(s):</b> Prof. Dr. S. Ikram A. Tirmizi, Dr. Sirajul Haq, Dr. M. Amer Qureshi</p>
			
			<h2>Computational Mathematics and Modeling</h2>
			<p>Computational mathematics involves mathematical research in areas of science where computing
			plays a central and essential role, emphasizing algorithms, numerical methods, and symbolic methods.
			Computation in the research is prominent. Computational mathematics emerged as a distinct part of
			applied mathematics. It is also concerned with constructing mathematical models and quantitative
			analysis techniques and using computers to analyze and solve scientific problems.</p>
			<p>Computation is the use of computing technology in information processing. Computation is a process
			following a well-defined model understood and expressed as, for example, an algorithm, or a protocol.</p>
			<p><b>Involved Faculty Member(s):</b> Prof. Dr. S. Ikram A. Tirmizi, Dr. M. Amer Qureshi</p>
			
			<h2>Photonics</h2>
			<p>The science of photonics includes the generation, emission, transmission, modulation, signal processing,
			switching, amplification, and detection/sensing of light. The term photonics thereby emphasizes that
			photons are neither particles nor waves — they are different in that they have both particle and wave
			nature. Applications of photonics Include all areas from everyday life to the most advanced science,
			e.g. light detection, telecommunications, information processing, lighting, metrology, spectroscopy,
			holography, medicine (surgery, vision correction, endoscopy, health monitoring), military technology,
			laser material processing, visual art, biophotonics, agriculture, and robotics.</p>
			<p><b>Involved Faculty Member(s):</b> Engr. Fahad Nawaz</p>
			
			<h2>Electro-Optics</h2>
			<p>Electro-optics is a branch of technology involving components, devices and systems which operate by
			modification of the optical properties of a material by an electric field. Thus it concerns the interaction
			between the electromagnetic (optical) and the electrical (electronic) states of materials.</p>
			<p><b>Involved Faculty Member(s):</b> >Engr. Salman Abdullah</p>
			
			<h2>Organic Semiconductors</h2>
			<p>An organic semiconductor is an organic material with semiconductor properties. Organic
			semiconductors are now-used as active elements in optoelectronic devices such as organic light-
			emitting diodes (OLED), organic solar cells, organic field-effect transistors (OFET), electrochemical
			transistors and recently in biosensing applications. Organic semiconductors have many advantages, such
			as easy fabrication, mechanical flexibility, and low cost.</p>
			<p><b>Involved Faculty Member(s):</b> Mr. Amer Elahi</p>
			
			<h2>Astrophysics</h2>
			<p>Astrophysics deals with the physics of the universe, including the physical properties of celestial objects,
			as well as their interactions and behavior.</p>
			<p><b>Involved Faculty Member(s):</b> Dr. Jamil-un-nabi</p>
			
			<h2>Siganls and Systems, Digial Signal Processing</h2>
			<p>The area of signals and systems includes continuous and discrete time systems, analysis of continuous (CT) systems using Fourier and Laplace transforms, ideal and practical CT filters, sampling, analysis of discrete time (DT) systems, difference equations and unit sample response, z-transform, DT Fourier transform.</p>
			<p>Digial Signal Processing involves discrete-time signals, sampling theory, interpolation and decimation, discrete-time Fourier transform, z-transform, Discrete Fourier Transform, Fast Fourier Transform digital filter design techniques, practical IIR and FIR filters, finite word length effects, introduction to discrete stochastic processes.</p>
			<p><b>Involved Faculty Member(s):</b> Engr. Rahim Umar</p>
			
			<h2>Electrical Engineering</h2>
			
			<p>Electrical engineering is a field of engineering that generally deals with the study and application
			of electricity, electronics, and electromagnetism. It now covers a wide range of subfields including
			electronics, digital computers, power engineering, telecommunications, control systems, RF
			engineering, and signal processing.</p>
			<p><b>Involved Faculty Member(s):</b> Engr. Rahim Umar, Engr. Noman Ahmed</p>
			
			<h2>Electronics Engineering</h2>
			<p>Electronics engineering is an engineering discipline where non-linear and active electrical components
			such as electron tubes, and semiconductor devices, especially transistors, diodes and integrated circuits,
			are utilized to design electronic circuits, devices and systems, typically also including passive electrical
			components and based on printed circuit boards.</p>
			<p><b>Involved Faculty Member(s):</b> Engr.Dr. Tahseen Amin Qasuria, Engr. Mehwish Khan</p>
			
			<h2>Computer Science</h2>
			<p>Computer science is the scientific and practical approach to computation and its applications.
			A computer scientist specializes in the theory of computation and the design of computers or
			computational systems.</p>
			<p><b>Involved Faculty Member(s):</b> Engr. Adeel Pervez</p>
			
			<h2>General Physics</h2>
			<p>Physics involves the study of matter and its motion through space and time, along with related concepts
			such as energy and force. More broadly, it is the general analysis of nature, conducted in order to
			understand how the universe behaves. Physics also makes significant contributions through advances in
			new technologies that arise from theoretical breakthroughs.</p>
			";
		   break;
		   
	case 'Research Labs':
	echo"
            <h1>Research Labs</h1>
			
			<h2>Thermal Analysis Laboratory</h2>
			<p>Has state-of-the-art equipment purchased from Perkin Elemer company such as Differential Scanning Calorimeter (Perkin Elemer DSC-7), Differential Thermal Analyser (DTA-7), Thermal Gravimetric Analyser (TGA-7) and Dynamic Mechanical Analyser (DMA-7).  The equipment can be used to investigate the kinetic parameters, and change of mass and mechanical properties of various materials with temperature.  Moreover, the equipment has direct application for the product development in the paper, ceramic, polymer, rubber, glass and paint industries.</p>
			
			<h2>Spectroscopy Laboratory</h2>
			<p>Contains Perkin Elemer, Fourier Transform, Infrared Spectrometer (FTIR System 2000) and UV/VIS/NIR (Spectrometer Lamda-19). Facilities are available for the spectroscopic analysis of liquid, solid and gaseous samples in transmission as well as reflection mode.  The equipment has direct applications in environmental studies, chemical, biochemical and pharmaceutical industries</p>
			
			<h2>Advanced Materials Synthesis and Processing Laboratory</h2>
			<p>Has state-of-the-art equipment purchased from Perkin Elemer company such as Differential Scanning Calorimeter (Perkin Elemer DSC-7), Differential Thermal Analyser (DTA-7), Thermal Gravimetric Analyser (TGA-7) and Dynamic Mechanical Analyser (DMA-7).  The equipment can be used to investigate the kinetic parameters, and change of mass and mechanical properties of various materials with temperature.  Moreover, the equipment has direct application for the product development in the paper, ceramic, polymer, rubber, glass and paint industries.</p>
			
			<h2>Simulation Laboratory</h2>
			<p>This lab has 10-networked Pentium 4, 2.2 GHz. PCs running Windows 2000/XP. The software available in this lab includes Matlab and SIMUL8. Matlab allows engineers to perform complex simulation testing. Dynamic system and mathematical models built using MATLAB/Simulink can be executed in real-time without the need for time-consuming programming. SIMUL8 lets engineers animate business processes. It is more than an animation as it works the same way as an operation, ideas can be tried out and changes can be implemented in a JIT 'just in time' environment. The Simulation Language used in the lab is VC.</p>
			
			<h2>Laser Research Laborartory</h2>
			<p>Contains Michelson interferometer kits, advanced optics kits, Newport fiber optics kits, spectrometers, DSP lock-in-amplifiers, fiber optics patch cards, WDM couplers, directional couplers, He-Ne lasers, Nd:YAG laser, diode lasers, laser power meters, PIN diodes, APDs, phototransistors, P IV computers, DAQ cards, 40 MHz Oscilloscopes, analog and digital trainers, a wide range of other electronic and optics components.</p>";
		   break;
	
	case 'Research Projects':
	echo"
            <h1>Research Projects</h1>
			<p>Details about research projects will be added soon.</p>";
		   break;

		   
}
?>