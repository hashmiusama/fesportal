$(document).ready(function() {
	
	/* Bootstraping variable */
	menu				= $('.buttons li');
	submenuWrapper	= $('#midmodule-static'); 
	submenu			= submenuWrapper.children('ul');
	firstSubmenu 	= submenu.eq(0);
	
	/* When menu on mouse over and out */
	menu.hover(
		function() {
			/*$(this).attr('id').substr(7,1)*/
			moveTo = $(this).index() * 25;
			showsubmenu(submenuWrapper);
			firstSubmenu.animate({'marginTop' : '-'+moveTo+'em' }, {duration: 200 });
		},
		
		function() { hidesubmenu(submenuWrapper); });
	
	/* When sub menu wrapper on mouse over and out */
	submenuWrapper.hover(
		function() { showsubmenu($(this)); },
		function() { hidesubmenu($(this));
	});
	
	/* Add focus on selected li *//*
	submenu
		.children('li')
		.hover(	function() { $(this).siblings().stop().animate({'opacity':'0.5'}); }, 
					function() { $(this).siblings().stop().animate({'opacity':'1'}); });
	
	/* Add focus on selected parent li *//*
	submenu
		.hover(	function() { menu.eq($(this).index()).addClass('selected')  },
					function() { menu.eq($(this).index()).removeClass('selected') });
	
	/* Function to show sub menu */
	function showsubmenu(item) {
		if(!item.hasClass('show'))
		
			item.addClass('show').stop().animate({'marginTop' : '0'}, {duration: 600 });
	}
	
	/* Function to hide sub menu */
	function hidesubmenu(item) {
		item.removeClass('show').stop().animate({'marginTop' : '-355px'},  {duration: 100 });
		
	}
	
});
!window.jQuery && document.write(unescape('%3Cscript src="jquery-1.7.1.min.js"%3E%3C/script%3E'))
