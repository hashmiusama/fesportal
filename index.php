<?php 
error_reporting(0);
include ('functions.php');
include ('access.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link type="text/css" rel="stylesheet" href="style.css" >
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="slides.js"></script>
<script type="text/javascript" src="slideshow.js"></script>
<script type="text/javascript" src="jquery-ui.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Faculty of Engineering Sciences, GIKI</title>
</head>
<body>
    <div id="main">
        <div id="slideshow">
            <img src="images/slideshow/Faculty.jpg" alt="Slideshow Image 2" class="active"/> 
            <img src="images/slideshow/Learning.jpg" alt="Slideshow Image 2" /> 
            <img src="images/slideshow/Research.jpg" alt="Slideshow Image 2" /> 
            <img src="images/slideshow/Labs.jpg" alt="Slideshow Image 2" /> 
            <img src="images/slideshow/Discussion.jpg" alt="Slideshow Image 2" /> 
            <img src="images/slideshow/Experimenting.jpg" alt="Slideshow Image 2" /> 
        </div><!--SLIDESHOW-->
		<div id="top">
			<div id="menu">
				<ul>
					<li><a>ABOUT FES</a>
						<ul>
							<li><a href="About FES/Overview.php">> OVERVIEW</a></li>
							<li><a href="About FES/MissionandObjectives.php">> MISSION AND OBJECTIVES</a></li>
							<li><a href="About FES/DeansMessage.php">> DEAN'S MESSAGE</a></li>
							<li><a href="About FES/WhatIsEngSci.php">> WHAT IS ENGINEERING SCIENCES?</a></li>
						</ul>
					</li>
					<li><a>ACADEMICS</a>
						<ul>
							<li><a href="Academics/EngSciProgram.php">> ENGINEERING SCIENCES PROGRAM</a></li>
							<li><a href="Academics/Courses.php">> COURSES</a></li>
							<li><a href="Academics/Specializations.php">> SPECIALIZATIONS</a></li>
							<li><a href="#">> GRADUATE PROGRAMS</a></li>
						</ul>
					</li>
					<li><a>FACULTY</a>
						<ul>
							<li><a href="Faculty/Dean.php">> DEAN</a></li>
							<li><a href="Faculty/Fulltime.php">> FULLTIME FACULTY</a></li>
							<li><a href="Faculty/ResearchAssociate.php">> RESEARCH ASSOCIATES</a></li>
							<li><a href="Faculty/Visiting.php">> VISITING FACULTY</a></li>
							<li><a href="Faculty/LabEngineers.php">> LAB ENGINEERS</a></li>
						</ul>
					</li>
					<li><a>CURRENT STUDENTS</a>
						<ul>
							<li><a href="CourseWare/">> STUDENT'S COURSEWARE</a></li>
							<li><a href="Students/DeansHonourRoll.php">> DEAN'S HONOUR ROLL</a></li>
							<li><a href="Students/BatchLists.php">> BATCH LISTINGS</a></li>
							<li><a href="Students/Projects.php">> PROJECTS</a></li>
							<li><a href="Students/Societies.php">> SOCIETIES</a></li>
						</ul>
					</li>
					<li><a>LABS AND RESEARCH</a>
						<ul>
							<li><a href="Laboratories/TeachingLabs.php">> TEACHING LABORATORIES</a></li>
							<li><a href="Laboratories/LabManuals.php">> STUDENT LAB MANUALS AND SOFTWARES</a></li>
							<li><a href="Research/ResearchLabs.php">> RESEARCH LABORATORIES</a></li>
							<li><a href="Research/ResearchAreas.php">> RESEARCH AREAS</a></li>
						</ul>
					</li>
					<li><a>INDUSTRY AND CAREERS</a>
						<ul>
							<li><a href="#">> CAREER OPTIONS</a></li>
							<li><a href="Industry And Careers/Internships.php">> INTERNSHIPS</a></li>
							<li><a href="#">> JOB OPPURTUNITITES</a></li>
							<li><a href="#">> FURTHER STUDY</a></li>
							<li><a href="Industry And Careers/Resumes.php">> RESUMES</a></li>
						</ul>
					</li>
					<li><a>FES COUNCIL</a>
						<ul>
							<li><a href="FES Council/Overview.php">> OVERVIEW</a></li>
							<li><a href="FES Council/ExecutiveCouncil.php">> EXECUTIVE COUNCIL</a></li>
							<li><a href="FES Council/Membership.php">> MEMBERSHIP</a></li>
						</ul>
					</li>
					<li><a>JOIN FES</a>
						<ul>
							<li><a href="#">> UNDERGRADUATE STUDENT</a></li>
							<li><a href="#">> GRADUATE STUDENT</a></li>
							<li><a href="#">> FACULTY MEMBER</a></li>
						</ul>
					</li>
				</ul>
			</div><!--MENU-->
			
			<!--TITLE-->
			<span class="primary">FACULTY OF</span><br>
			<span class="primary">ENGINEERING SCIENCES</span><br>
			<span class="secondary">GHULAM ISHAQ KHAN INSTITUTE OF ENGINEERING SCIENCES AND TECHNOLOGY</span>
		</div><!--TOP-->
		<div id="bottom">
			<div class="slidebox">		
				<div class="header"></div><!--HEADER-->
				<div class="slide"></div><!--SLIDE-->
			</div><!--SLIDEBOX-->
			<div class="tabs">
				<ul>
						<li><a href="Bulletin/NewsAndAnnouncements.php">NEWS</a></li>
						<li><a href="Bulletin/Seminars.php">SEMINARS</a></li>
						<li><a href="CourseWare/">STUDENT'S COURSEWARE</a></li>
						<li><a href="admin/">FACULTY PANEL</a></li>
				</ul>
			</div><!--TABS-->
		</div><!--BOTTOM-->
		<div id="footer">
			<p>COPYRIGHT © 2013. FACULTY OF ENGINEERING SCIENCES, GIK INSTITUTE. ALL RIGHT RESERVED. <br />WEB PORTAL DEVELOPED BY <b>MUHAMMAD USAMA </b> (u2010252@giki.edu.pk)</p>
		</div><!--FOOTER-->
	</div><!--MAIN-->
</body>
<?php 
include('db_connect.php');
$slides=mysql_query("SELECT * FROM `fes`.`news_seminars` WHERE `status`='1' ORDER BY `id` DESC") ;
$slide=mysql_fetch_array($slides);
$i=0;
echo'
<script>
var slide = new Array();';
do{
echo"
	slide[".$i."] = '<a href=".'"'.'Bulletin/'.$slide['type'].'.php?id='.$slide['id'].'"'."><span>".$slide['title']."</span></a><p>";if($slide['photo']){echo'<img src="images/bulletin/'.$slide['photo'].'.jpg" />';} echo $slide['detail']."</p>'";
$i++;
}
while($slide=mysql_fetch_array($slides));
echo'
</script>
';
?>
</html>