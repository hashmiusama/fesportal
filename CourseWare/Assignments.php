<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='course';
$page='Assignments';
$tablename='assignments';

require('php/functions.php');
if($_SESSION['current course code']){
	$pagetitle=$_SESSION['current course code'].' - '.$_SESSION['current course title'];
}

//extract entries from database
$entries=mysql_query("SELECT * FROM `".$_SESSION['current course code']."`.`".$tablename."` ORDER BY `row id` DESC");
$entry=mysql_fetch_array($entries);

?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper">
				<h1>Assignments</h1>
				<?php if($_SESSION['course error']){
				echo '<h2>'.$_SESSION['course error'].'</h2>';
				}
				else{
				echo'<div id="sliding-list">';
				do
				{
					if(!$entry)
					{
					echo '
					<h2>No assignment uploaded.</h2>';
					} 
					else 
					{
					echo'
					<li class="closeitem expandable">'.$entry['title'].'</li>
					<ul class="listcontent">
						<li><h2>Title:</h2><p>'.$entry['title'].'</p></li>';
						if($entry['deadline'])
						{
						echo'<li><h2>Deadline:</h2><p>'.$entry['deadline'].'</p></li>';
						}
						if($entry['task'])
						{
						echo'<li><h2>Task:</h2><p>'.$entry['task'].'</p></li>';
						}
						if($entry['details'])
						{
						echo'<li><h2>Details:</h2><p>'.$entry['details'].'</p></li>';
						}
						if($entry['file'])
						{
						echo'
						<li><h2>Attachment:</h2><p><a href="../CourseWare/files/'.$_SESSION['current course code'].'/'.$entry['url'].'">'.$entry['file'].'</a></p></li>';
						}
						echo'<li><span class="posted">Posted on '.$entry['posted'].'</span></li>
					</ul>';				
					}
				}
				while ($entry = mysql_fetch_array($entries));
								
				echo'
				</div><!--sliding-list-->';}?>
			
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$_SESSION['current course code']=NULL;
$_SESSION['course error']=NULL;

//destroy session current course
include('php/foot.php'); ?>
