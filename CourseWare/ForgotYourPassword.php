<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='lobby';
$page='Forgot Your Password';
$tablename1='forgotrequest';
$tablename='users';

require('php/functions.php');
$pagetitle='Student CourseWare';

//$faculties = array( array(title => 'Select Faculty...', value=>'0'), array(title => 'Faculty of Engineering Sciences', value=>'fes'), array(title => 'Faculty of Electronic Engineering', value=>'fee'), array(title => 'Faculty of Mechanical Engineering', value=>'fme'	), array(title => 'Faculty of Materials Engineering', value=>'fmse'), array(title => 'Faculty of Computer Engineering', value=>'fce'), array(title => 'Faculty of Computer Science', value=>'fcse')); 


//actions
if($_POST['submit'])
{ 	$forgot['email']=mysql_real_escape_string($_POST['email']);
	$forgot['code']=mysql_real_escape_string($_POST['code']);
	
	$checkemail = "/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";

	if(!$forgot['email'] || !preg_match($checkemail, $forgot['email']))
	{
		$error['1']='*Please enter your valid email.';
	}	
	if($forgot['code']!=$_SESSION['code'])
	{
		$error['2']='*Please enter the code shown on left correctly.';
	}
	if(!$error)
	{
		
		$user_select_query = mysql_query("SELECT * FROM `fes`.`".$tablename."` WHERE `email` = '".$forgot['email']."' LIMIT 1;");
		$user_found = mysql_num_rows($user_select_query);
		if($user_found == 0)
		{
			$error['1']='The email you entered is incorrect. Please enter the correct email.';
		}
		else
		{
			$user=mysql_fetch_array($user_select_query);
			if(!$error)
			{
				$forgot['forgotcode'] = $user['password'];
				$forgot['ip'] = getRealIpAddr();
				$forgot['time'] = date("d/m/y : H:i:s", time());
				
				$forgot_request_select = mysql_query("SELECT * FROM `fes`.`".$tablename1."` WHERE `email` = '".$forgot['email']."'");
				$forgot_request_found = mysql_num_rows($forgot_request_select);
				if($forgot_request_found == 1)
				{
					$create_forgot_request=mysql_query("UPDATE`fes`.`".$tablename1."` SET `username`='".$user['username']."', `forgotcode`='".$user['password']."', `ip`='".$forgot['ip']."', `time`='".$forgot['time']."' WHERE `email`='".$forgot['email']."'");
				}
				else
				{
					$create_forgot_request=mysql_query("INSERT INTO `fes`.`".$tablename1."` (`username`, `email`, `forgotcode`, `ip`, `time`) VALUES('".$user['username']."','".$forgot['email']."','".$user['password']."','".$forgot['ip']."','".$forgot['time']."')");
				}
				
				if($create_forgot_request)
				{
					$subject = "Password Reset Request";
					$message = "Dear ".$user['name'].", 

You have requested a reset of your password. 

Your username: ".$user['username']."

Click the URL below to reset your password for Student CourseWare Portal of Faculty of Engineering Sciences: 
http://192.168.172.222/CourseWare/ResetPassword.php?request=".$forgot['forgotcode']."

Regards,
Webmaster FES";
					
					$header = "From:webmaster.fes@gmail.com \r\n";
					$forgot_mail = mail ($forgot['email'],$subject,$message,$header);
					
					if($forgot_mail == true)  
					{
						$forgot['forgotrequest']="Your username and a password reset link have been sent at <b>".$forgot['email']."</b>. Please check your email.";
					}
					else
					{
						$forgot['forgotrequest']="Sorry, we were enable to email you the password reset link. Please try again later.";
					}
				}
				else
				{
					$forgot['forgotrequest']="Sorry, can not proceed your request at the moment. Please try again later.";
				}
				$_POST=NULL;
	
			}
		}
	}
}


$_SESSION['code']=rand(156712,987679)

?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper">
				<h1>Forgot Your Password?</h1>
				<div id="form-wrapper">
				<?php 
				if(!$forgot['forgotrequest'])
				{
				echo'	
					<table>
						<form action="" method="post" enctype="multipart/form-data">
							<span class="label">Enter your email to get username and password reset link:</span>
							<tr><td><span class="label">Email:</span></td><td><input type="text" name="email" value="'.$_POST['email'].'"><span class="message"> '.$error['1'].'</span></td></tr>
							<tr><td><span class="label">Enter the Code Shown:</span></td><td><input type="text" name="code"><span class="label" style="font-size:16px;"><b> '.$_SESSION['code'].'</b></span><span class="message"> '.$error['2'].'</span></td></tr>
							<tr><td></td><td><input class="button" type="submit" name="submit" value="Submit" /><input class="button" type="reset" name="reset" value="Reset" /></td></tr>
						</form>
					</table>';
					}
				else if($forgot['forgotrequest'])
					{
					echo'
							<span class="label">'.$forgot['forgotrequest'].'</span>';
					}
					
				?>		
				</div><!--form-wrapper-->
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$error=NULL;
include('php/foot.php'); ?>