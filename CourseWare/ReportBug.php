<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='Go to';
$page='Report A Bug';
$tablename='bug';

require('php/functions.php');
$pagetitle='Report Bug';

//actions
if($_POST['add'])
{ 

	$add['title']=mysql_real_escape_string($_POST['title']);
	$add['bug details']=mysql_real_escape_string($_POST['bug_details']);
	$add['name']=$_SESSION['user name'];
	$add['username']=$_SESSION['user username'];
	$add['ip']=getRealIpAddr();

	if(!$add['title'])
	{
		$_SESSION['error']='Please enter a title report bug.';
	}
	else if(!$add['bug details'])
	{
		$_SESSION['error']='Please enter bug details to report bug.';
	}
	else
	{	
			$add_query=mysql_query("INSERT INTO `fes`.`".$tablename."` (`title`, `details`, `name`, `username`, `ip`, `posted`) VALUES('".$add['title']."','".$add['bug details']."','".$add['name']."','".$add['username']."','".$add['ip']."','".date("d/m/y : H:i:s", time())."')");
			$_SESSION['success']='Thank you for reporting the bug. We will soon look into it.';
			$_POST=NULL;
			header('refresh:0'); die();
	}
}


//Assigning session messages to local message variable
$error=$_SESSION['error'];
$warning=$_SESSION['warning'];
$success=$_SESSION['success'];

$_SESSION['error']=NULL;
$_SESSION['warning']=NULL;
$_SESSION['success']=NULL;

?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper">
				<h1>Found a Bug?</h1>
				<div id="form-wrapper"> 
				<h2>Fill out the form to report the bug:</h2>
					<?php if($error){ 
					echo'<span class="message"><b>ERROR:</b> '.$error.'</span>';}
					if($warning){ 
					echo'<span class="message"><b>WARNING:</b> '.$warning.'</span>';}
					if($success){ 
					echo'<span class="message">'.$success.'</span>';}
					echo'
					<table>
						<form action="" method="post" enctype="multipart/form-data">
							<tr><td width="135"><span class="label">Title:</span></td><td><input type="text" name="title" value="'.$_POST['title'].'"></td></tr>
							<tr><td><span class="label">Bug Details:</span></td><td><textarea name="bug_details">'.$_POST['bug_details'].'</textarea></td></tr>
							<tr><td></td><td><input class="button" type="submit" name="add" value="Submit" /><input class="button" type="reset" name="reset" value="Reset" /></td></tr>
						</form>
					</table>';
					?>
				</div><!--form-wrapper-->
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$_SESSION['current course code']=NULL;
$error=NULL;
$warning=NULL;
$success=NULL;
//destroy session current course
include('php/foot.php'); ?>
