<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='edit profile';
$page='Basic Details';
$tablename='users';

require('php/functions.php');
$pagetitle='Edit Profile';

$faculties = array( array(title => 'Select Faculty...', value=>'0'), array(title => 'Faculty of Engineering Sciences', value=>'fes'), array(title => 'Faculty of Electronic Engineering', value=>'fee'), array(title => 'Faculty of Mechanical Engineering', value=>'fme'	), array(title => 'Faculty of Materials Engineering', value=>'fmse'), array(title => 'Faculty of Chemical Engineering', value=>'fcse'), array(title => 'Faculty of Computer Engineering', value=>'fce'), array(title => 'Faculty of Computer Science', value=>'fcse')); 



//actions
if($_POST['save'])
{ 
	$save['name']=mysql_real_escape_string($_POST['name']);
	$save['faculty']=mysql_real_escape_string($_POST['faculty']);
	$save['batch']=mysql_real_escape_string($_POST['batch']);
	$save['reg']=mysql_real_escape_string($_POST['reg']);
	$save['dob']=mysql_real_escape_string($_POST['dob']);
	$save['email']=mysql_real_escape_string($_POST['email']);
	$save['phone']=mysql_real_escape_string($_POST['phone']);
	
	$checkemail = "/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";

	if(!$save['name'])
	{
		$error['1']='*Please enter your full name.';
	}
	if(!$save['faculty'])
	{
		$error['2']='*Please select your faculty.';
	}
	if(!$save['batch'])
	{
		$error['3']='*Please select your batch.';
	}
	if(!$save['reg'])
	{
		$error['4']='*Please enter your GIKI registration number.';
	}
	if(!$save['dob'])
	{
		$error['5']='*Please enter your date of birth.';
	}
	if(!$save['email'] || !preg_match($checkemail, $save['email']))
	{
		$error['6']='*Please enter a valid email.';
	}	
	if(!$save['phone'])
	{
		$error['7']='Please enter your cell phone number.';
	}
	if(!$error)
	{
		$check_email_query = mysql_query("SELECT * FROM `fes`.`".$tablename."` WHERE `email` = '".$save['email']."'");
		$check_email=mysql_fetch_array($check_email_query);
		$num1 = mysql_num_rows($check_email_query);
		if($num1 == 1 && $check_email['username']!=$_SESSION['user username'])
		{
			$error['6']='*The email you entered is already taken.';
		}
		if(!$error)
		{
			$save_query_users=mysql_query("UPDATE `fes`.`".$tablename."` SET `name` = '".$save['name']."', `faculty` = '".$save['faculty']."', `batch` = '".$save['batch']."', `reg` = '".$save['reg']."', `dob` = '".$save['dob']."', `email` = '".$save['email']."', `phone` = '".$save['phone']."' WHERE `username`='".$_SESSION['user username']."' LIMIT 1;");
		
			$_SESSION['user name'] = $save['name'];
			$_SESSION['user email'] = $save['email'];
			$_SESSION['user reg'] = $save['reg'];
			
			
			$_SESSION['success']='Changes have been saved.';
			$_POST=NULL;
			header('refresh:0'); die();
		}
	}
}

//extract entries from database
$user_select_query=mysql_query("SELECT * FROM `fes`.`".$tablename."` WHERE `username`='".$_SESSION['user username']."' LIMIT 1;");
$user=mysql_fetch_array($user_select_query);

if($_POST['edit'])
{ 
	$_POST['name']=$user['name'];
	$_POST['faculty']=$user['faculty'];
	$_POST['batch']=$user['batch'];
	$_POST['reg']=$user['reg'];
	$_POST['dob']=$user['dob'];
	$_POST['email']=$user['email'];
	$_POST['phone']=$user['phone'];
}

//Assigning session messages to local message variable
$warning=$_SESSION['warning'];
$success=$_SESSION['success'];

$_SESSION['error']=NULL;
$_SESSION['warning']=NULL;
$_SESSION['success']=NULL;

?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper">
				<h1>Basic Details</h1>
				<div id="form-wrapper">
				<?php 
				if($warning){ 
					echo'<span class="message"><b>WARNING:</b> '.$warning.'</span>';}
				if($success){ 
					echo'<span class="message">'.$success.'</span>';}
					
				echo'	
					<table>
						<form action="" method="post" enctype="multipart/form-data">';
					if($_POST['edit'] || $_POST['save'])
					{
					echo'
							<tr><td width="135px"><span class="label">Username:</span></td><td><span class="label">'.$user['username'].'</span></td></tr>
							<tr><td><span class="label">Full Name:</span></td><td><input type="text" name="name" value="'.$_POST['name'].'"><span class="message"> '.$error['1'].'</span></td></tr>
							
							<tr><td><span class="label">Faculty:</span></td><td><select name="faculty" id="faculty">';
							foreach ($faculties as $faculty)
							{
								echo "<option ";
								if($faculty['value'] == $_POST['faculty']) { echo "selected='selected' ";} 
								echo "value=".$faculty['value'].">".$faculty['title']."</option>";  
							}
							echo '</select><span class="message"> '.$error['2'].'</span></td></tr>
							<tr><td><span class="label">Batch:</span></td><td><select name="batch" id="batch"><option value="0">Select Batch...</option>';
							for ($i=1 ; $i<=23 ; $i++)
							{
								echo "<option ";
								if($i == $_POST['batch']) { echo "selected='selected' ";} 
								echo "value=".$i.">".$i."</option>";  
							}
							echo '</select><span class="message"> '.$error['3'].'</span></td></tr>
							<tr><td><span class="label">GIKI Registration Number:</span></td><td><input type="text" name="reg" value="'.$_POST['reg'].'"><span class="message"> '.$error['4'].'</span></td></tr>
							<tr><td><span class="label">Date of Birth:</span></td><td><input type="text" name="dob" value="'.$_POST['dob'].'"><span class="label"> Format: YYYY-MM-DD</span><span class="message"> '.$error['5'].'</span></td></tr>
							<tr><td><span class="label">Email:</span></td><td><input type="text" name="email" value="'.$_POST['email'].'"><span class="message"> '.$error['6'].'</span></td></tr>
							<tr><td><span class="label">Cell Phone:</span></td><td><input type="text" name="phone" value="'.$_POST['phone'].'"><span class="message"> '.$error['7'].'</span></td></tr>
							<tr><td></td><td><input class="button" type="submit" name="save" value="Save" /><input class="button" type="reset" name="reset" value="Reset" /></td></tr>';
					}
					else if(!$_POST['edit'] && !$_POST['save'])
					{
					echo'
							<tr><td width="135px"><span class="label">Username:</span></td><td><span class="label">'.$user['username'].'</span></td></tr>
							<tr><td><span class="label">Full Name:</span></td><td><span class="label">'.$user['name'].'</span></td></tr>
							<tr><td><span class="label">Faculty:</span></td><td><span class="label">';
							foreach ($faculties as $faculty)
							{
								if($faculty['value'] == $user['faculty']) { echo $faculty['title'];} 
							}
							echo '</span></td></tr>
							<tr><td><span class="label">Batch:</span></td><td><span class="label">'.$user['batch'].'</span></td></tr>
							<tr><td><span class="label">GIKI Registration Number:</span></td><td><span class="label">'.$user['reg'].'</span></td></tr>
							<tr><td><span class="label">Date of Birth:</span></td><td><span class="label">'.$user['dob'].'</span></td></tr>
							<tr><td><span class="label">Email:</span></td><td><span class="label">'.$user['email'].'</span></td></tr>
							<tr><td><span class="label">Cell Phone:</span></td><td><span class="label">'.$user['phone'].'</span></td></tr>
							<tr><td></td><td><input class="button" type="submit" name="edit" value="Edit" /></td></tr>';
					}
				?>

						</form>
					</table>
				</div><!--form-wrapper-->
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$error=NULL;
$warning=NULL;
$success=NULL;
include('php/foot.php'); ?>
