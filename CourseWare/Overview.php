<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='course';
$page='Overview';
$tablename='overview';
$tablename2='announcements';

require('php/functions.php');
if($_SESSION['current course code']){
	$pagetitle=$_SESSION['current course code'].' - '.$_SESSION['current course title'];
}

//extract entries from database
$entries=mysql_query("SELECT * FROM `".$_SESSION['current course code']."`.`".$tablename."`");
$entry=mysql_fetch_array($entries);

$announcements=mysql_query("SELECT * FROM `".$_SESSION['current course code']."`.`".$tablename2."` ORDER BY `row id` DESC");
$announcement=mysql_fetch_array($announcements);
?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper">
				<h1>Overview</h1>
				<?php if($_SESSION['course error']){
				echo '<h2>'.$_SESSION['course error'].'</h2>';
				}
				else{
				echo'<div id="form-wrapper">		
					<table>
						<tr><td width="135px"><span class="label">Course Code:</span></td><td><span class="label">'.$_SESSION['current course code'].'</span></td></tr>
						<tr><td><span class="label">Course Title:</span></td><td><span class="label">'.$_SESSION['current course title'].'</span></td></tr>
						<tr><td><span class="label">Instructor:</span></td><td><span class="label">'.$entry['instructor'].'</span></td></tr>
						<tr><td><span class="label">Credit Hours:</span></td><td><span class="label">'.$entry['credit hours'].'</span></td></tr>
						<tr><td><span class="label">Course Description:</span></td><td><span class="label">'.$entry['course description'].'</span></td></tr>
						<tr><td><span class="label">Pre-requisite:</span></td><td><span class="label">'.$entry['pre requisite'].'</span></td></tr>
						<tr><td><span class="label">Class Schedule:</span></td><td><span class="label">'.$entry['class schedule'].'</span></td></tr>
						<tr><td><span class="label">Teaching Assistant:</span></td><td><span class="label">'.$entry['teaching assistant'].'</span></td></tr>
						<tr><td><span class="label">Grading Policy:</span></td><td><span class="label">'.$entry['grading policy'].'</span></td></tr>
					</table>
				</div><!--form-wrapper-->';
				echo'
				<h1>Announcements</h1>
				<div id="sliding-list">';
				do
				{
					if(!$announcement)
					{
					echo '
					<h2>No announcements added.</h2>'; 
					} 
					else 
					{
					echo'
					<li class="closeitem expandable">'.$announcement['title'].'</li>
					<ul class="listcontent">
						<li><h2>Title:</h2><p>'.$announcement['title'].'</p></li>';
						if($announcement['details'])
						{
						echo'
						<li><h2>Details:</h2><p>'.$announcement['details'].'</p></li>';
						}if($announcement['file'])
						{
						echo'
						<li><h2>Attachment:</h2><p><a href="../CourseWare/files/'.$_SESSION['current course code'].'/'.$announcement['url'].'">'.$announcement['file'].'</a></p></li>';
						}
						echo'<li><span class="posted">Posted on '.$announcement['posted'].'</span></li>	
					</ul>';				
					}
				}
				while ($announcement=mysql_fetch_array($announcements));
								
				echo'
				</div><!--sliding-list-->';}?>
			
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$_SESSION['current course code']=NULL;
$_SESSION['course error']=NULL;

//destroy session current course
include('php/foot.php'); ?>
