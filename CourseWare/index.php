<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='lobby';
$page='index';
$tablename='users';
$tablename2='coursesoffered';
$tablename3='coursesenrolled';

require('php/functions.php');
$pagetitle='Student CourseWare';

//actions
if($_POST['login']){
	
	$login['username']=mysql_real_escape_string($_POST['username']);
	$login['password']=mysql_real_escape_string($_POST['password']);
	$login['password']=md5($login['password']);
	
	if(!$login['username'])
	{
		$error='Please enter Username';
    }
	else if(!$login['password'])
	{
		$error='Please enter Password';
    }
	else
	{
		$user_select_query = mysql_query("SELECT * FROM `fes`.`".$tablename."` WHERE `username` = '".$login['username']."'");
		$user_found = mysql_num_rows($user_select_query);
		if($user_found == 0)
		{
			$error='The Username is invalid. Please enter the correct Username.';
		}
		else
		{
			$user=mysql_fetch_array($user_select_query);
			if($login['password']!=$user['password'])
			{
				$error='The Password is invalid. Please enter the correct Password.';
			}
			else
			{
				$_SESSION['user id'] = $user['id'];
				$_SESSION['user name'] = $user['name'];
				$_SESSION['user username'] = $user['username'];
				$_SESSION['user email'] = $user['email'];
				$_SESSION['user reg'] = $user['reg'];
				
				$_SESSION['user login']=1;
				
				$login['lastonline'] = date("d/m/y : H:i:s", time());
				$last_online_update_query=mysql_query("UPDATE `fes`.`".$tablename."` SET `last online` = '".$login['lastonline']."' WHERE `username`='".$_SESSION['user username']."' LIMIT 1;");

				header('Location: index.php');
			}
		}
	}
}
if(isset($_POST['join']))
{
		
	$select_course=mysql_query("SELECT * FROM `fes`.`".$tablename2."` WHERE `course title` = '".$_POST['coursetojoin']."'");
	$join = mysql_fetch_array($select_course);
	
	if(!$join)
	{
		$error= 'Please select a course.';
	}
	else{
	
		$select_joined_courses = mysql_query("SELECT * FROM `fes`.`".$tablename3."` WHERE `id` = '".$_SESSION['user id']."' AND `username` = '".$_SESSION['user username']."'"); 
		$num = mysql_num_rows($select_joined_courses);
		if($num > 14)
		{
			$error= 'You can not exceed the maximum course limit (Maximum Course Limit: 15)';
		}
		else
		{
			$find_course = mysql_query("SELECT * FROM `fes`.`".$tablename3."` WHERE `id` = '".$_SESSION['user id']."' AND `username` = '".$_SESSION['user username']."' AND `course code` = '".$join['course code']."'"); 
			$num = mysql_num_rows($find_course);
			if($num >= 1)
			{
				$error= 'You have already joined the selected course.';

			}
			else{
				$join_course = mysql_query("INSERT INTO `fes`.`".$tablename3."` (`id`, `username`, `reg`, `name`, `email`, `course code`, `course title`) VALUES('".$_SESSION['user id']."','".$_SESSION['user username']."','".$_SESSION['user reg']."','".$_SESSION['user name']."','".$_SESSION['user email']."','".$join['course code']."','".$join['course title']."')");

			}
		}
			
	}
}
	
if ($_SESSION['user login']==1)
{
	$select_joined_courses=mysql_query("SELECT * FROM `fes`.`".$tablename3."` WHERE `id` = '".$_SESSION['user id']."' AND `username` = '".$_SESSION['user username']."' ");
	$select_course_list=mysql_query("SELECT * FROM `fes`.`coursesoffered`");

}

?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper"><?php
				if($_SESSION['user login']==1)
				{
echo'
					<h1>Welcome '.$_SESSION['user name'].',</h1>	
					<div id="text-wrapper" class="half">
						<p>Students CourseWare is an online system for students of Faculty of Engineering Sciences, that makes creation, management, and use of course materials and communication among students and instructors more effective and efficient.</p>
						<p>This CourseWare will allow you to stay updated will the proceedings of the class. All the announcements of quizzes, details of assignments and projects, attendance  and other material related to course will be posted by course instructors on this portal. You will be also able to complete course evaluation form online at the end of semester through this portal.</p>
						<p>You can follow any of the courses offered by Faculty of Engineering Sciences in current semester, by joining the course below.</p>
					</div><!--text-wrapper-->
					
					<div id="courses-joined">
						<h2>Courses Joined</h2>
						<ul>';
						$joined_courses = mysql_fetch_array($select_joined_courses);
						do{
							if(!$joined_courses)
							{
								echo '<a><li>No courses joined.</li></a>';
							} 
							else {
								echo '<a href="overview.php?course='.$joined_courses['course code'].'"><li>'. $joined_courses['course title'].'</li></a>';
							}
						}while ($joined_courses = mysql_fetch_array($select_joined_courses));
echo					'</ul>
					</div><!--courses-joined-->
					
					<div id="form-wrapper" class="half">
						<h2>Join a Course:</h2>
						<span class="message">'.$error.$success.'</span>							
						<table>
							<form action="" method="post">
							<span class="label">Course: </span>
							<select name="coursetojoin" id="coursetojoin">
							<option value="0">Select Course...</option>';  
							$course_list = mysql_fetch_array($select_course_list);
							do{
echo			 				'<option value="'.$course_list['course title'].'">'.$course_list['course code'].' - '.$course_list['course title'].'</option>';
							}while ($course_list = mysql_fetch_array($select_course_list));
echo							'</select>
							<input class="button" type="submit" name="join" value="Join">
							</form>
						</table>
					</div><!--form-wrapper-->
					';
				}
				else
				{
				echo '
				<h1>Login Here</h1>
				<div id="text-wrapper">
				<p>Students CourseWare is an online system for students of Faculty of Engineering Sciences, that makes creation, management, and use of course materials and communication among students and instructors more effective and efficient.</p>
				</div><!--text-wrapper-->
				<div id="login-wrapper"> 
					<h2>Please enter your Username and Password</h2>
					<span class="message">'.$error.'</span>
					<table>
						<form action="" method="post" enctype="multipart/form-data">
						<tr><td><span class="label">Username:</span></td><td><input type="text" name="username" value="'.$_POST['username'].'"></td></tr>
						<tr><td><span class="label">Password:</span></td><td><input type="password" name="password"></td></tr>
						<tr><td></td><td><input class="button" type="submit" name="login" value="Login" /><input class="button" type="reset" name="reset" value="Reset" /></td></tr>
						<tr><td></td><td><a href="ForgotYourPassword.php"style="color:#555;"><span class="label">Forgot your password?</span></a></td></tr>
						</form>
					</table>
				</div><!--login-wrapper-->
				';
				}
				?>
			
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$error=NULL;
include('php/foot.php'); ?>
