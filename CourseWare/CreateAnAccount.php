<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='lobby';
$page='Create an Account';
$tablename='users';

require('php/functions.php');
$pagetitle='Student CourseWare';

$faculties = array( array(title => 'Select Faculty...', value=>'0'), array(title => 'Faculty of Engineering Sciences', value=>'fes'), array(title => 'Faculty of Electronic Engineering', value=>'fee'), array(title => 'Faculty of Mechanical Engineering', value=>'fme'	), array(title => 'Faculty of Materials Engineering', value=>'fmse'), array(title => 'Faculty of Chemical Engineering', value=>'fcse'), array(title => 'Faculty of Computer Engineering', value=>'fce'), array(title => 'Faculty of Computer Science', value=>'fcse')); 


//actions
if($_POST['submit'])
{ 
	$user['username']=mysql_real_escape_string($_POST['username']);
	$user['password']=mysql_real_escape_string($_POST['password']);
	$user['confirmpassword']=mysql_real_escape_string($_POST['confirmpassword']);
	$user['email']=mysql_real_escape_string($_POST['email']);
	$user['name']=mysql_real_escape_string($_POST['name']);
	$user['faculty']=mysql_real_escape_string($_POST['faculty']);
	$user['batch']=mysql_real_escape_string($_POST['batch']);
	$user['reg']=mysql_real_escape_string($_POST['reg']);
	$user['dob']=mysql_real_escape_string($_POST['dob']);
	$user['phone']=mysql_real_escape_string($_POST['phone']);
	$user['code']=mysql_real_escape_string($_POST['code']);
	
	$checkemail = "/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";

	if(!$user['username'])
	{
		$error['1']='*Please enter a username.';
	}
	if(!$user['password'])
	{
		$error['2']='*Please enter a password.';
	}
	if(!$user['confirmpassword'])
	{
		$error['3']='*Please enter the password again.';
	}
	if(!$user['name'])
	{
		$error['5']='*Please enter your full name.';
	}
	if(!$user['faculty'])
	{
		$error['6']='*Please select your faculty.';
	}
	if(!$user['batch'])
	{
		$error['7']='*Please select your batch.';
	}
	if(!$user['reg'])
	{
		$error['8']='*Please enter your GIKI registration number.';
	}
	if(!$user['dob'])
	{
		$error['9']='*Please enter your date of birth.';
	}
	if(!$user['email'] || !preg_match($checkemail, $user['email']))
	{
		$error['4']='*Please enter your valid email.';
	}	
	if(!$user['phone'])
	{
		$error['10']='*Please enter your cell phone number.';
	}
	if($user['code']!=$_SESSION['code'])
	{
		$error['11']='*Please enter the code shown on left correctly.';
	}
	if($user['username'] && (strlen($user['username']) < 4 || strlen($user['username']) > 32))
	{
		$error['1']='*The username must be 4-32 characters long.';
	}
	if($user['password'] && (strlen($user['password']) < 4 || strlen($user['password']) > 32))
	{
		$error['2']='*The password must be 4-32 characters long.';
	}
	if($user['password'] && $user['confirmpassword'] && $user['password']!=$user['confirmpassword'])
	{
		$error['3']='*The password you entered do not match, please enter again.';
	}
	if(!$error)
	{
		$check_username_query = mysql_query("SELECT * FROM `fes`.`".$tablename."` WHERE `username` = '".$user['username']."'");
		$num = mysql_num_rows($check_username_query);
		if($num == 1)
		{
			$error['1']='*The username you entered is already taken.';
		}
		
		$check_email_query = mysql_query("SELECT * FROM `fes`.`".$tablename."` WHERE `email` = '".$user['email']."'");
		$num1 = mysql_num_rows($check_email_query);
		if($num1 == 1)
		{
			$error['4']='*The email you entered is already taken.';
		}
			
		if(!$error)
		{
			$user['ip'] = getRealIpAddr();
			$user['created'] = date("d/m/y : H:i:s", time());
						
			$user['password']=md5($user['password']);
			$create_account_query=mysql_query("INSERT INTO `fes`.`".$tablename."` (`username`, `password`, `email`, `name`, `faculty`, `batch`, `reg`, `dob`, `phone`, `ip`, `created`) VALUES('".$user['username']."','".$user['password']."','".$user['email']."','".$user['name']."','".$user['faculty']."','".$user['batch']."','".$user['reg']."','".$user['dob']."','".$user['phone']."','".$user['ip']."','".$user['created']."')");
			$user['registered']=1;
		
		}
	}
}

$_SESSION['code']=rand(156712,987679)

?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper">
				<h1>Create an Account</h1>
				<div id="form-wrapper">
				<?php 
				if(!$user['registered'])
				{
				echo'	
					<table>
						<form action="" method="post" enctype="multipart/form-data">
							<tr><td width="165px"><span class="label">Username:</span></td><td><input type="text" name="username" value="'.$_POST['username'].'"><span class="message"> '.$error['1'].'</span></td></tr>
							<tr><td><span class="label">Password:</span></td><td><input type="password" name="password"><span class="message"> '.$error['2'].'</span></td></tr>
							<tr><td><span class="label">Confirm Passowrd:</span></td><td><input type="password" name="confirmpassword"><span class="message"> '.$error['3'].'</span></td></tr>
							<tr><td><span class="label">Email:</span></td><td><input type="text" name="email" value="'.$_POST['email'].'"><span class="message"> '.$error['4'].'</span></td></tr>
							<tr><td><span class="label">Full Name:</span></td><td><input type="text" name="name" value="'.$_POST['name'].'"><span class="message"> '.$error['5'].'</span></td></tr>
							
							<tr><td><span class="label">Faculty:</span></td><td><select name="faculty" id="faculty">';
							foreach ($faculties as $faculty)
							{
								echo "<option ";
								if($faculty['value'] == $_POST['faculty']) { echo "selected='selected' ";} 
								echo "value=".$faculty['value'].">".$faculty['title']."</option>";  
							}
							echo '</select><span class="message"> '.$error['6'].'</span></td></tr>
							<tr><td><span class="label">Batch:</span></td><td><select name="batch" id="batch"><option value="0">Select Batch...</option>';
							for ($i=1 ; $i<=23 ; $i++)
							{
								echo "<option ";
								if($i == $_POST['batch']) { echo "selected='selected' ";} 
								echo "value=".$i.">".$i."</option>";  
							}
							echo '</select><span class="message"> '.$error['7'].'</span></td></tr>
							<tr><td><span class="label">GIKI Registration Number:</span></td><td><input type="text" name="reg" value="'.$_POST['reg'].'"><span class="message"> '.$error['8'].'</span></td></tr>
							<tr><td><span class="label">Date of Birth:</span></td><td><input type="text" name="dob" value="'.$_POST['dob'].'"><span class="label"> Format: YYYYMMDD</span><span class="message"> '.$error['9'].'</span></td></tr>
							<tr><td><span class="label">Cell Phone:</span></td><td><input type="text" name="phone" value="'.$_POST['phone'].'"><span class="message"> '.$error['10'].'</span></td></tr>
							<tr><td><span class="label">Enter the Code Shown:</span></td><td><input type="text" name="code"><span class="label" style="font-size:16px;"><b> '.$_SESSION['code'].'</b></span><span class="message"> '.$error['11'].'</span></td></tr>
							<tr><td></td><td><input class="button" type="submit" name="submit" value="Submit" /><input class="button" type="reset" name="reset" value="Reset" /></td></tr>
						</form>
					</table>';
					}
					else if($user['registered'])
					{
					echo'
							<span class="label">Your account has been created. Wait while you are being redirected to <a href="index.php">Login Page<a></span>';
							header('refresh:4;url="index.php"');
					}
				?>		
				</div><!--form-wrapper-->
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$error=NULL;
include('php/foot.php'); ?>
