<?php
error_reporting(0);
include ('db_connect.php');
include ('functions.php');
include ('access.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $sectionheading;?> - Faculty of Engineering Sciences, GIKI</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<link href="../mid-module.css" rel="stylesheet" type="text/css">
<link href="../rightCol.css" rel="stylesheet" type="text/css">
<?php if($pageheading=='Courses'){echo 
'<link href="courses.css" rel="stylesheet" type="text/css">
';}
if($sectionheading=='Faculty'){echo 
'<link href="faculty.css" rel="stylesheet" type="text/css">
';}?>
<script type="text/javascript" src="../jquery.js"></script>
<script type="text/javascript" src="../jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../mid-module.js"></script>
<script type="text/javascript" src="../dimensions.js"></script>
<script type="text/javascript" src="../accordion.js"></script>
<?php if($partition==1){echo 
'<script type="text/javascript" src="../rightCol.js"></script>
';}?>
<?php if($pageheading=='Courses' || $sectionheading=='Faculty'){echo 
'<script type="text/javascript" src="../listaccordion.js"></script>
';}?>
</head>
<body>
<div id="main">
	<div id="header">
    	<div id="header-wrapper">
        	<div id="headertitle"></div><!-- headertitle-->
            <div id="headerpanel">
                <?php include('headerpanel.php');?>
            </div><!-- headerpanel-->
    	</div><!-- header-wrapper-->
    </div><!-- header-->
    <div id="main-menu">
    	<?php include('menu.php');?>
    </div><!-- menu-->
		
    <div id="module-wrapper">
    	<?php  include('mid-module.php');?>
    </div><!-- module-wrapper-->
            
    <div id="bodybox">
       	<div id="content-wrapper">
        	
            <div id="content-titlebox"><span><?php echo $sectionheading;?></span></div>
            
            <?php if($partition==1){
			echo"
            <div id='content-textbox'>";}else{
			echo"
            <div id='content-textbox' class='full'>";}?>
				<!-- page content-->
				<?php include($sectionheading.'/content.php');?>
                <!-- page content-->
            </div>
            
            
            <?php if($partition==1){
			echo" <div id='right-wrapper'>";
			include('rightCol.php');
			echo
			"</div>";}?>
			
            
        </div><!--content-wrapper-->
    </div><!-- bodybox-->

    <?php include('footer.php');?>

</div><!--main-->    
</body>
</html>