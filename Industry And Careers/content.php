<?php echo     	'';
     switch($pageheading){
	case 'Career Options':
	echo"
          	<h1>Career Options</h1>
			";
		   break;
		   
	case 'Further Study':
	echo"
            <h1>Further Study</h1>
			";
			break;	
	
	case 'Internships':
	echo"
            <h1>Internships</h1>
			
			<h2>Batch 20</h2>
			<p><a href='internships/ES_20_Internships.pdf'>Click Here</a> to download internship placement list of batch 20.</p>
			
			<h2>Batch 19</h2>
			<p><a href='internships/ES_19_Internships.pdf'>Click Here</a> to download internship placement list of batch 19.</p>
			
			<h2>Batch 18</h2>
			<p><a href='internships/ES_18_Internships.pdf'>Click Here</a> to download internship placement list of batch 18.</p>		
			";
			break;	
	
	case 'Job Oppurtunities':
	echo"
            <h1>Job Oppurtunities</h1>
			<p>Job Oppurtunities will be posted soon.</p>";
			break;	
			
	case 'Resumes':
	echo"
            <h1>Resumes</h1>
			
			<h2>Batch 19 - Class of 2013</h2>
			<p><a href='resumes/es19/ES_19_Resumes.zip'>Click Here</a> to download all resumes of batch 19.</p>
			<table class='inner'>
			<tr><td width='300px'>Abbas Hussain Bangash</td><td width='75px'><a href='resumes/es19/Abbas Hussain Bangash CV.docx'>Download</a></td></tr>
			<tr><td>Ahmed Kamal</td><td><a href='resumes/es19/Ahmed Kamal CV.pdf'>Download</a></td></tr>
			<tr><td>Aleena Humayun</td><td><a href='resumes/es19/Aleena Humayun CV.pdf'>Download</a></td></tr>
			<tr><td>Ali Raza</td><td><a href='resumes/es19/Ali Raza CV.pdf'>Download</a></td></tr>
			<tr><td>Ammad Ilyas</td><td><a href='resumes/es19/Ammad CV.pdf'>Download</a></td></tr>
			<tr><td>Asad Munir</td><td><a href='resumes/es19/Asad CV.doc'>Download</a></td></tr>
			<tr><td>Bahawal Haq</td><td><a href='resumes/es19/Bahawal Haq CV.pdf'>Download</a></td></tr>
			<tr><td>Faiz Soomro</td><td><a href='resumes/es19/FaizSoomro CV.pdf'>Download</a></td></tr>
			<tr><td>Hamza Malik</td><td><a href='resumes/es19/Hamza Malik CV.docx'>Download</a></td></tr>
			<tr><td>Hassan Manzoor</td><td><a href='resumes/es19/Hassan Manzoor CV.doc'>Download</a></td></tr>
			<tr><td>Muhammad Ali Abbas</td><td><a href='resumes/es19/Muhammad Ali Abbas CV.docx'>Download</a></td></tr>
			<tr><td>Nouman Zia</td><td><a href='resumes/es19/Nouman Zia CV.pdf'>Download</a></td></tr>
			<tr><td>Sedef Shakoor Amjad</td><td><a href='resumes/es19/Sedef CV.pdf'>Download</a></td></tr>
			<tr><td>Shehla Hayat</td><td><a href='resumes/es19/Shehla Hayat CV.docx'>Download</a></td></tr>
			<tr><td>Syed Ahmed Shah</td><td><a href='resumes/es19/Syed Ahmed Shah CV.docx'>Download</a></td></tr>
			<tr><td>Syed Muhammad Saeed Kazmi</td><td><a href='resumes/es19/Syed Muhammad Saeed Kazmi CV.docx'>Download</a></td></tr>
			<tr><td>Tahir Jamal</td><td><a href='resumes/es19/Tahir Jamal CV.docx'>Download</a></td></tr>
			<tr><td>Wajahat Umar</td><td><a href='resumes/es19/Wajahat Umer CV.docx'>Download</a></td></tr>
			<tr><td>Wasay Muzamil</td><td><a href='resumes/es19/Wasay Muzamil CV.docx'>Download</a></td></tr>
			<tr><td>Yousuf Riaz</td><td><a href='resumes/es19/Yousaf Riaz CV.pdf'>Download</a></td></tr>
			<tr><td>Yousuf Hemani</td><td><a href='resumes/es19/Yousuf Hemani CV.pdf'>Download</a></td></tr>
			<tr><td>Zaid ur Rehman</td><td><a href='resumes/es19/Zaid ur Rehman CV.pdf'>Download</a></td></tr>
			<tr><td>Zeeshan Jora</td><td><a href='resumes/es19/Zeeshan Jora CV.pdf'>Download</a></td></tr>
			</table>
			
			<h2>Batch 18 - Class of 2012</h2>
			<p><a href='resumes/es18/ES_18_Resumes.zip'>Click Here</a> to download all resumes of batch 18.</p>
			<table class='inner'>
			<tr><td width='300px'>Abdul Raffay Sarwar</td><td width='75px'><a href='resumes/es18/Abdul Raffay Sarwar CV.DOC'>Download</a></td></tr>
			<tr><td>Ahsan Naseer</td><td><a href='resumes/es18/Ahsan Naseer CV.pdf'>Download</a></td></tr>
			<tr><td>Ammar Hussain</td><td><a href='resumes/es18/Ammar CV.pdf'>Download</a></td></tr>
			<tr><td>Asad Jan</td><td><a href='resumes/es18/Asad CV.pdf'>Download</a></td></tr>
			<tr><td>Bassam Riaz</td><td><a href='resumes/es18/Bassam Riaz CV.docx'>Download</a></td></tr>
			<tr><td>Bilal Pervaiz</td><td><a href='resumes/es18/Bilal Pervaiz CV.docx'>Download</a></td></tr>
			<tr><td>Hadi Shahbaz</td><td><a href='resumes/es18/Hadi Shahbaz CV.pdf'>Download</a></td></tr>
			<tr><td>Sohaib Khan</td><td><a href='resumes/es18/Sohaib Khan CV.pdf'>Download</a></td></tr>
			<tr><td>Usama Ali Bhatti</td><td><a href='resumes/es18/Usama CV.pdf'>Download</a></td></tr>
			<tr><td>Zain ul Abidin</td><td><a href='resumes/es18/Zain ul Abidin CV.pdf'>Download</a></td></tr>
			<tr><td>Zain ul Hassan</td><td><a href='resumes/es18/Zain ul Hassan CV.pdf'>Download</a></td></tr>
			</table>
			";
		   break;
		
	
	
	
			
}
?>