<?php echo     	'';
     switch($pageheading){
	case "Dean's Honour Roll":
	echo"
          	<h1>Dean's Honour Roll</h1>
			<p>Dean's honor roll includes students who show outstanding academic performance in the course of one
semester. Students with a GPA of 3.5 or higher are given certificates of appreciation by the Dean, in
appreciation of their hard work. This not only motivates the recipients of this honor to maintain their
exceptional academic performance, but also motivates other students to improve their performance
and to aim for a better GPA.</p>

			<h2>Spring 2012</h2>
			
			<table class='inner'>
			<tr><td width='75px'><b>Reg. No</b></td><td width='160px' class='inner'><b>Name</b></td><td width='100px'><b>Semester GPA</b></td></tr>
			<tr><td>2009031</td><td>Aleena Humayun</td><td>3.50</td></tr>
			<tr><td>2010042</td><td>Ali Nawaz Babar</td><td>3.62</td></tr>
			<tr><td>2010293</td><td>Arslan Raja</td><td>3.73</td></tr>
			<tr><td>2009078</td><td>Faiz Hassan Soomro</td><td>3.50</td></tr>
			<tr><td>2010151</td><td>Junaid Imtiaz</td><td>x.xx</td></tr>
			<tr><td>2010251</td><td>Umer Mujadid</td><td>x.xx</td></tr>
			<tr><td>2010308</td><td>Wajahat Umer</td><td>3.50</td></tr>
			</table>
			
			<h2>Fall 2012</h2>
			<table class='inner'>
			<tr><td width='75px'><b>Reg. No</b></td><td width='160px' class='inner'><b>Name</b></td><td width='100px'><b>Semester GPA</b></td></tr>
			<tr><td>2009031</td><td>Aleena Humayun</td><td>3.80</td></tr>
			<tr><td>2009038</td><td>Ali Raza</td><td></td></tr>
			<tr><td>2009048</td><td>Asad Munir</td><td>3.64</td></tr>
			<tr><td>2009061</td><td>Bahawal Haq</td><td>3.91</td></tr>
			<tr><td>2009078</td><td>Faiz Hassan Soomro</td><td>3.51</td></tr>
			<tr><td>2009156</td><td>Muhammad Ali Abbas</td><td>3.71</td></tr>
			<tr><td>2009218</td><td>Noman Zia</td><td>3.51</td></tr>
			<tr><td>2009250</td><td>Sana Khan</td><td></td></tr>
			<tr><td>2010042</td><td>Ali Nawaz Babar</td><td>3.93</td></tr>
			<tr><td>2010078</td><td>Attique ur Rehman</td><td>3.52</td></tr>
			<tr><td>2010151</td><td>Malik Aqib Rehman</td><td>3.59</td></tr>
			<tr><td>2010293</td><td>Raja Arslan Sajid</td><td>3.80</td></tr>
			<tr><td>2010339</td><td>Sohaib Ahmed Nasir</td><td>3.52</td></tr>
			<tr><td>2010403</td><td>Syed Ali Haider</td><td>3.52</td></tr>
			</table>			
			
			<h2>Spring 2013</h2>
			
			<table class='inner'>
			<tr><td width='75px'><b>Reg. No</b></td><td width='160px' class='inner'><b>Name</b></td><td width='100px'><b>Semester GPA</b></td></tr>
			<tr><td>2009031</td><td>Aleena Humayun</td><td>4.00</td></tr>
			<tr><td>2009038</td><td>Ali Raza</td><td>3.73</td></tr>
			<tr><td>2009048</td><td>Asad Munir</td><td>3.67</td></tr>
			<tr><td>2009061</td><td>Bahawal Haq</td><td>3.89</td></tr>
			<tr><td>2009078</td><td>Faiz Hassan Soomro</td><td>3.53</td></tr>
			<tr><td>2009156</td><td>Muhammad Ali Abbas</td><td>3.67</td></tr>
			<tr><td>2009218</td><td>Noman Zia</td><td>3.73</td></tr>
			<tr><td>2009250</td><td>Sana Khan</td><td>3.80</td></tr>
			<tr><td>2009264</td><td>Shehla Hayat</td><td>3.53</td></tr>
			<tr><td>2009308</td><td>Wajahat Umer</td><td>3.53</td></tr>
			<tr><td>2010078</td><td>Attique ur Rehman</td><td>3.53</td></tr>
			<tr><td>2010293</td><td>Raja Arslan Sajid</td><td>3.78</td></tr>
			<tr><td>2011022</td><td>Aftab Ahmed</td><td>3.50</td></tr>
			<tr><td>2011076</td><td>Bushra Jamal</td><td>3.63</td></tr>
			<tr><td>2011172</td><td>Khuzema Sunel</td><td>3.67</td></tr>
			<tr><td>2011242</td><td>Muhammad Ghawas</td><td>3.61</td></tr>
			<tr><td>2011403</td><td>Syed Ali Haider</td><td>3.52</td></tr>
			</table>
			";
		   break;
		   
	case 'Batch Listings':
	echo"
            <h1>Batch Listings</h1>
			
			<h2>Batch 19 - Class of 2013</h2>
			
			
			<table class='inner'>
			<tr><td width='235px'><b>Name</b></td><td width='130px'><b>Reg. No</b></td><td width='130px'><b>Email</b></td></tr>
			<tr><td><table style='margin:0px;'>
				<tr><td>Abbas Hussain Bangash</td></tr>
				<tr><td>Ahmed kamal</td></tr>
				<tr><td>Aleena Humayun</td></tr>
				<tr><td>Ali Raza</td></tr>
				<tr><td>Ammad Ilyas</td></tr>
				<tr><td>Asad Munir</td></tr>
				<tr><td>Asfantagin Khan</td></tr>
				<tr><td>Bahawal Haq</td></tr>
				<tr><td>Faiz Hassan Somro</td></tr>
				<tr><td>Hamza Malik</td></tr>
				<tr><td>Hasan Manzoor Chaudhry</td></tr>
				<tr><td>Immad Abdul Razzaq</td></tr>
				<tr><td>Mian Fahad Ahmad</td></tr>
				<tr><td>Moez Aslam Butt</td></tr>
				<tr><td>Mohammad Yousaf</td></tr>
				<tr><td>Muhammad Ali Abbas</td></tr>
				<tr><td>Muhammad Hamza Faisal</td></tr>
				<tr><td>Muhammad Nouman Ahmed</td></tr>
				<tr><td>Muhammad Tahir Jamal</td></tr>
				<tr><td>Mustafa Rauf</td></tr>
				<tr><td>Naveed Ullah</td></tr>
				<tr><td>Nouman Zia</td></tr>
				<tr><td>Nuasabbeh Waqar</td></tr>
				<tr><td>Saad Ahmad</td></tr>
				<tr><td>Sana Khan</td></tr>
				<tr><td>Saud Imran</td></tr>
				<tr><td>Sedef Shakoor Amjad</td></tr>
				<tr><td>Shehla Hayat</td></tr>
				<tr><td>Syed Ahmad Shah</td></tr>
				<tr><td>Syed Muhammad Saeed Kazmi</td></tr>
				<tr><td>Syed Wasay Muzamil</td></tr>
				<tr><td>Umair Zeb</td></tr>
				<tr><td>Usama Hayat Malik</td></tr>
				<tr><td>Usman Tariq</td></tr>
				<tr><td>Wajahat Umer</td></tr>
				<tr><td>Yousuf Hemani</td></tr>
				<tr><td>Zaid Ur Rehman</td></tr>
				<tr><td>Zeeshan Ahmad Jora</td></tr>
				<tr><td>Zia Ur Rehman</td></tr>
			</table></td><td><table style='margin:0px;'>
				<tr><td>2009003</td></tr>
				<tr><td>2009021</td></tr>
				<tr><td>2009031</td></tr>
				<tr><td>2009038</td></tr>
				<tr><td>2009039</td></tr>
				<tr><td>2009048</td></tr>
				<tr><td>2009049</td></tr>
				<tr><td>2009061</td></tr>
				<tr><td>2009078</td></tr>
				<tr><td>2009093</td></tr>
				<tr><td>2009098</td></tr>
				<tr><td>2009109</td></tr>
				<tr><td>2009135</td></tr>
				<tr><td>2009138</td></tr>
				<tr><td>2009142</td></tr>
				<tr><td>2009156</td></tr>
				<tr><td>2009167</td></tr>
				<tr><td>2009177</td></tr>
				<tr><td>2009187</td></tr>
				<tr><td>2009212</td></tr>
				<tr><td>2009217</td></tr>
				<tr><td>2009218</td></tr>
				<tr><td>2009219</td></tr>
				<tr><td>2009231</td></tr>
				<tr><td>2009250</td></tr>
				<tr><td>2009253</td></tr>
				<tr><td>2009255</td></tr>
				<tr><td>2009264</td></tr>
				<tr><td>2009275</td></tr>
				<tr><td>2009278</td></tr>
				<tr><td>2009284</td></tr>
				<tr><td>2009295</td></tr>
				<tr><td>2009301</td></tr>
				<tr><td>2009304</td></tr>
				<tr><td>2009308</td></tr>
				<tr><td>2009316</td></tr>
				<tr><td>2009318</td></tr>
				<tr><td>2009321</td></tr>
				<tr><td>2009325</td></tr>
			</td></table><td><table style='margin:0px;'>
				<tr><td>u2009003@giki.edu.pk</td></tr>
				<tr><td>u2009021@giki.edu.pk</td></tr>
				<tr><td>u2009031@giki.edu.pk</td></tr>
				<tr><td>u2009038@giki.edu.pk</td></tr>
				<tr><td>u2009039@giki.edu.pk</td></tr>
				<tr><td>u2009048@giki.edu.pk</td></tr>
				<tr><td>u2009049@giki.edu.pk</td></tr>
				<tr><td>u2009061@giki.edu.pk</td></tr>
				<tr><td>u2009078@giki.edu.pk</td></tr>
				<tr><td>u2009093@giki.edu.pk</td></tr>
				<tr><td>u2009098@giki.edu.pk</td></tr>
				<tr><td>u2009109@giki.edu.pk</td></tr>
				<tr><td>u2009135@giki.edu.pk</td></tr>
				<tr><td>u2009138@giki.edu.pk</td></tr>
				<tr><td>u2009142@giki.edu.pk</td></tr>
				<tr><td>u2009156@giki.edu.pk</td></tr>
				<tr><td>u2009167@giki.edu.pk</td></tr>
				<tr><td>u2009177@giki.edu.pk</td></tr>
				<tr><td>u2009187@giki.edu.pk</td></tr>
				<tr><td>u2009212@giki.edu.pk</td></tr>
				<tr><td>u2009217@giki.edu.pk</td></tr>
				<tr><td>u2009218@giki.edu.pk</td></tr>
				<tr><td>u2009219@giki.edu.pk</td></tr>
				<tr><td>u2009231@giki.edu.pk</td></tr>
				<tr><td>u2009250@giki.edu.pk</td></tr>
				<tr><td>u2009253@giki.edu.pk</td></tr>
				<tr><td>u2009255@giki.edu.pk</td></tr>
				<tr><td>u2009264@giki.edu.pk</td></tr>
				<tr><td>u2009275@giki.edu.pk</td></tr>
				<tr><td>u2009278@giki.edu.pk</td></tr>
				<tr><td>u2009284@giki.edu.pk</td></tr>
				<tr><td>u2009295@giki.edu.pk</td></tr>
				<tr><td>u2009301@giki.edu.pk</td></tr>
				<tr><td>u2009304@giki.edu.pk</td></tr>
				<tr><td>u2009308@giki.edu.pk</td></tr>
				<tr><td>u2009316@giki.edu.pk</td></tr>
				<tr><td>u2009318@giki.edu.pk</td></tr>
				<tr><td>u2009321@giki.edu.pk</td></tr>
				<tr><td>u2009325@giki.edu.pk</td></tr>
			</td></table></tr>
			</table>
			
			<h2>Batch 20 - Class of 2014</h2>
			
			
			<table class='inner'>
			<tr><td width='235px'><b>Name</b></td><td width='130px'><b>Reg. No</b></td><td width='130px'><b>Email</b></td></tr>
			<tr><td>Ahmed Hassan Qureshi</td><td>2010025</td><td>u2010025@giki.edu.pk</td><tr>
			<tr><td>Ahsan Ali</td><td>2010032</td><td>u2010032@giki.edu.pk</td><tr>  
			<tr><td>Ali Nawaz Babar</td><td>2010042</td><td>u2010042@giki.edu.pk</td><tr>  
			<tr><td>Aqeel Ahmed </td><td>2010061</td><td>u2010061@giki.edu.pk</td><tr>  
			<tr><td>Arslan Khawar</td><td>2010065</td><td>u2010065@giki.edu.pk</td><tr> 
			<tr><td>Asad Abbas </td><td>2010066</td><td>u2010066@giki.edu.pk</td><tr>  
			<tr><td>Asad Hayat</td><td>2010068</td><td>u2010068@giki.edu.pk</td><tr>  
			<tr><td>Attique Ur Rehman</td><td>2010078</td><td>u2010078@giki.edu.pk</td><tr>   
			<tr><td>Badar Naeem</td><td>2010082</td><td>u2010082@giki.edu.pk</td><tr>   
			<tr><td>Bilal Ahmad</td><td>2010086</td><td>u2010086@giki.edu.pk</td><tr>   
			<tr><td>Daniyal Basit</td><td>2010092</td><td>u2010092@giki.edu.pk</td><tr>   
			<tr><td>Fahad Zulfiqar</td><td>2010097</td><td>u2010097@giki.edu.pk</td><tr>   
			<tr><td>Fazal E Rehman</td><td>2010108</td><td>u2010108@giki.edu.pk</td><tr>   
			<tr><td>Hassan Yar</td><td>2010127</td><td>u2010127@giki.edu.pk</td><tr>   
			<tr><td>Jahanzaib Tariq</td><td>2010145</td><td>u2010145@giki.edu.pk</td><tr>   
			<tr><td>Jamal Gulzar</td><td>2010147</td><td>u2010147@giki.edu.pk</td><tr>   
			<tr><td>Junaid Imtiaz</td><td>2010151</td><td>u2010151@giki.edu.pk</td><tr>   
			<tr><td>Mahin Anwar</td><td>2010161</td><td>u2010161@giki.edu.pk</td><tr>  
			<tr><td>Malik Aqib Rehman</td><td>2010164</td><td>u2010164@giki.edu.pk</td><tr>   
			<tr><td>Muhammad Bahjut Afroz </td><td>2010197</td><td>u2010197@giki.edu.pk</td><tr>   
			<tr><td>Muhammad Faris Khan</td><td>2010206</td><td>u2010206@giki.edu.pk</td><tr>   
			<tr><td>Muhammad Saqib</td><td>2010237</td><td>u2010237@giki.edu.pk</td><tr>   
			<tr><td>Muhammad Umar Mujaddid</td><td>2010251</td><td>u2010251@giki.edu.pk</td><tr>   
			<tr><td>Muhammad Usama</td><td>2010252</td><td>u2010252@giki.edu.pk</td><tr>  
			<tr><td>Muhammad Usman Asghar Rai</td><td>2010256</td><td>u2010256@giki.edu.pk</td><tr>   
			<tr><td>Muhammad Usman</td><td>2010254</td><td>u2010254@giki.edu.pk</td><tr>   
			<tr><td>Muhammad Waqas</td><td>	2010261</td><td>u2010261@giki.edu.pk</td><tr>   
			<tr><td>Naveed Ahmad</td><td>2010278</td><td>u2010278@giki.edu.pk</td><tr>   
			<tr><td>Raja Arslan Sajid</td><td>2010293</td><td>u2010293@giki.edu.pk</td><tr>   
			<tr><td>Raza Bashir Shah</td><td>2010296</td><td>u2010296@giki.edu.pk</td><tr>   
			<tr><td>Saad Ur Rehman</td><td>2010302</td><td>u2010302@giki.edu.pk</td><tr>   
			<tr><td>Seerat Ul Urooj</td><td>2010324</td><td>u2010324@giki.edu.pk</td><tr>   
			<tr><td>Semab Neimat Khan</td><td>2010325</td><td>u2010325@giki.edu.pk</td><tr>   
			<tr><td>Sohaib Ahmed Nasir</td><td>	2010339</td><td>u2010339@giki.edu.pk</td><tr>  
			<tr><td>Syed Osama Safdar</td><td>2010352</td><td>u2010352@giki.edu.pk</td><tr>   
			<tr><td>Usama Khursheed Zafar</td><td>2010373</td><td>u2010373@giki.edu.pk</td><tr>   
			<tr><td>Yamna Anwar</td><td>2010386</td><td>u2010386@giki.edu.pk</td><tr>   
			<tr><td>Zohaib Hanif</td><td>2010399</td><td>u2010399@giki.edu.pk</td><tr>   
			</table>";
		   break;
		
		   
	case 'Projects':
	echo"
            <h1>Projects</h1>
			<p>Details about student projects will be added soon.</p>";
			break;
	
	case 'Societies':
	echo"
            <h1>Societies</h1>
			<h2>SPIE – Society for Photo-Optical Instrumentation Engineers</h2>
			<p>The SPIE (International Society for Photo-optical Instrumentation Engineers) is the largest international force for the exchange, collection and dissemination of knowledge in optics, photonics and imaging. As a newly emerging society, the SPIE GIKI chapter is the growing legacy of those who seek to learn, discover and innovate by creating, promoting and sustaining a multifaceted platform for Photonics, Photo-optics and Instrumentation. The GIKI chapter also has strong associations with both indigenous and foreign companies and academic/technical collaboration in the form of seminars and workshops is a frequent experience for members. Regular visits and tours, to some of the latest engineering complexes, are conducted to facilitate hands-on experience. Working in conjunction with the administration, the SPIE GIKI chapter has also had the privilege of managing and hosting the Open House for the past 3 years.</p>
			
			<h2>GMS – GIK Institute Mathematics Society </h2>
			<p>GIK Institute Mathematics Society is an established society of GIK Institute for the encouragement of study and appreciation of Mathematics among perspective engineers. We believe in developing enthusiasm about Mathematics in students which will lead to their contribution in it. GMS takes pride in being the first society ever in any Engineering institute in Pakistan. GMS has events all around the year that makes it one of the most active societies in GIKI. These activities are spread over a range of modules and from research based projects to quiz competitions, seminars, workshops and social events. GMS also tends to develop enthusiasm and an aesthetic sense for Mathematics in students of GIKI and participants from different universities through such events. Our work has also been recognized internationally by prestigious associations like American Mathematics Society (AMS) which donated Rs. 0.5 million worth of books to the GMS. These are placed at the institute's Central Library for the benefit of all students.</p>";
			break;
			

		   
}
?>