<?php echo     	'';
     switch($pageheading){
	case 'Overview':
	echo"
          	<h1>Overview</h1>
			<p>The purpose of  the FES Mentoring Council (hereafter referrred to as Council) is to assist 
the Faculty of Engineering Sciences in mentoring of its students. The Council will work as 
a facilitating agent to improve the  overall academic performance of the  students, 
specially the weak students, of the ES program. The Council would also assist and guide 
the students about the future prospects  and research carried out in the field of 
engineering sciences.</p>

			<h2>Agenda of Council</h2>
			<p>> Arranging tutorials and other guidance sessions for weak students.<br>
			> Monthly/weekly meeting with mandatory attendance to discuss academic progress of weak students and report to the advisor for necessary action.<br>
			> Assisting the FES with managing FYP's (proper listing of projects, re-use of the former FYP's by junior batch) in guidance from the Dean, FES. <br>
			> Assisting the FES with career counseling and Open House & Career Fair events (Internships / jobs) in guidance from the Dean, FES.<br>
			> Coordinating seminars (invited speakers from outside GIKI) in guidance from the Dean, FES.<br>
			> Publicity (Promotional videos, CD's, Posters) regarding ES program.<br>
			> Managing book bank.<br>
			> Managing Alumni forum for ES students.</p>";
		   break;
		   
	case 'Executive Council':
	echo"
            <h1>Executive Council</h1>
			<h2>Council Advisor</h2>
			<p>Prof. Dr. Jameel-Un Nabi (Dean)</p>
			
			<h2>Executive Body 2013-14</h2>
			<table class='inner'>
			<tr><td width='200px'>Ali Nawaz Babar</td><td width='250px'>President</td></tr>
			<tr><td>Sohaib Ahmed Nasir</td><td>Director External Relations</td></tr>
			<tr><td>Muhammad Usman</td><td>Director Internal Relations</td></tr>
			<tr><td>Raja Arslan Sajid</td><td>Director Academics</td></tr>
			<tr><td>Fahad Zulfiquar</td><td>Technical Head</td></tr>
			</table>
			
			<h2>Executive Body 2012-13</h2>
			<table class='inner'>
			<tr><td width='200px'>Faiz Hassan Soomro</td><td width='250px'>President</td></tr>
			<tr><td>Zaid-ur-Rehman</td><td>Director External Relations</td></tr>
			<tr><td>Bahawal Haq</td><td>Director Academics</td></tr>
			<tr><td>Nouman Zia</td><td>Technical Head</td></tr>
			</table>";
		   break;
		
		   
	case 'Membership':
	echo"
            <h1>Membership</h1>
			<p>The eligibility requirement for becoming a member of the Council is to 
possess a good moral character and a  minimum CGPA  of 2.5. Exceptional 
cases with lower CGPA would require formal approval from Executive Council 
and the Advisor.</p>
			<p>Inductions for Batch 21 will be held in November/December 2012.</p>";


	  

			
		   break;
		   
}
?>