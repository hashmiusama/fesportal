<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='account';
$page='Upload Photo';
$tablename='profiles';

require('php/functions.php');
$pagetitle='Edit Profile';

//actions
if($_POST['upload'])
{ 
	$add['file']=$_FILES["file"]["name"];
	
	if(!$add['file'])
	{
		$_SESSION['error']='Please choose a photo to upload.';
	}
	else
	{	
		$function_return=uploadphoto($add['file'],$_SESSION['faculty name']);
			
		$_SESSION['warning']=$function_return['warning'];
		$_SESSION['error']=$function_return['error'];
			
		if(!$_SESSION['error'])
		{
			$_SESSION['success']='New photo has been uploaded.';
			$_POST=NULL;
			header('refresh:0'); die();
		}
	}
}

//Assigning session messages to local message variable
$error=$_SESSION['error'];
$warning=$_SESSION['warning'];
$success=$_SESSION['success'];

$_SESSION['error']=NULL;
$_SESSION['warning']=NULL;
$_SESSION['success']=NULL;

?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper">
				<h1>Upload Photo</h1>
				<div id="form-wrapper">
				<?php 
				if($error){ 
					echo'<span class="message"><b>ERROR:</b> '.$error.'</span>';}
				if($warning){ 
					echo'<span class="message"><b>WARNING:</b> '.$warning.'</span>';}
				if($success){ 
					echo'<span class="message">'.$success.'</span>';}
				?>	
					<table>
						<form action="" method="post" enctype="multipart/form-data">
							<tr><td width="135px"><span class="label">Your Current Photo:</span></td><td><img src="../faculty/photos/<?php echo $_SESSION['faculty name'];?>.jpg" width="190px"></td></tr>
							<tr><td><span class="label">Upload New Photo:</span></td><td><input class="file" type="file" name="file" id="file" /><br /><span class="label">Format Allowed: JPEG</span><br /><span class="label">Max File Size: 150MB</span></td></tr>
							<tr><td></td><td><input class="button" type="submit" name="upload" value="Upload" /></td></tr>
						</form>
					</table>
				</div><!--form-wrapper-->
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$error=NULL;
$warning=NULL;
$success=NULL;
include('php/foot.php'); ?>
