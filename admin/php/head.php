<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="Faculty Panel" />
<meta name="author" content="Web Engineer - Faculty of Engineering Sciences" />
<title><?php echo $pagetitle;?></title>
<link href="css/styles.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/listaccordion.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
</head>
<body>
	<div id="panel-wrapper"></div><!--panel-wrapper-->
	<div id="panel">
		<div id="panel-top">
		</div><!--panel-top-->
		<div id="main-menu">
			<?php include('php/menu.php'); ?>
		</div><!-- main-menu-->
	</div><!--panel-->
	<div id="body-wrapper">
		<div id="body-top"><span class="page-title"><?php echo $pagetitle;?></span></div><!--body-top-->
		<div id="body-main">
