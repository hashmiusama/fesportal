<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='edit profile';
$page='Basic Details';
$tablename='profiles';

require('php/functions.php');
$pagetitle='Edit Profile';

//actions
if($_POST['save'])
{ 
	$save['name']=mysql_real_escape_string($_POST['name']);
	$save['qualification']=mysql_real_escape_string($_POST['qualification']);
	$save['office']=mysql_real_escape_string($_POST['office']);
	$save['email']=mysql_real_escape_string($_POST['email']);
	$save['phone']=mysql_real_escape_string($_POST['phone']);
	
	if(!$save['name'])
	{
		$_SESSION['error']='Please enter your name.';
	}
	else
	{
		$save_query_profiles=mysql_query("UPDATE `fes`.`".$tablename."` SET `name` = '".$save['name']."', `qualification` = '".$save['qualification']."', `office` = '".$save['office']."', `email` = '".$save['email']."', `phone` = '".$save['phone']."' WHERE `username`='".$_SESSION['faculty username']."' LIMIT 1;"); 
		$save_query_faculty=mysql_query("UPDATE `fes`.`faculty` SET `name` = '".$save['name']."' WHERE `username`='".$_SESSION['faculty username']."' LIMIT 1;");
		$_SESSION['success']='Changes have been saved.';
		$_POST=NULL;
		header('refresh:0'); die();
	}
}

//extract entries from database
$faculty_select_query=mysql_query("SELECT * FROM `fes`.`".$tablename."` WHERE `username`='".$_SESSION['faculty username']."' LIMIT 1;");
$faculty=mysql_fetch_array($faculty_select_query);

if($_POST['edit'])
{ 
	$_POST['name']=$faculty['name'];
	$_POST['qualification']=$faculty['qualification'];
	$_POST['office']=$faculty['office'];
	$_POST['email']=$faculty['email'];
	$_POST['phone']=$faculty['phone'];
}

//Assigning session messages to local message variable
$error=$_SESSION['error'];
$warning=$_SESSION['warning'];
$success=$_SESSION['success'];

$_SESSION['error']=NULL;
$_SESSION['warning']=NULL;
$_SESSION['success']=NULL;

?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper">
				<h1>Basic Details</h1>
				<div id="form-wrapper">
				<?php 
				if($error){ 
					echo'<span class="message"><b>ERROR:</b> '.$error.'</span>';}
				if($warning){ 
					echo'<span class="message"><b>WARNING:</b> '.$warning.'</span>';}
				if($success){ 
					echo'<span class="message">'.$success.'</span>';}
					
				echo'	
					<table>
						<form action="" method="post" enctype="multipart/form-data">';
					if($_POST['edit'] || $_POST['save'])
					{
					echo'
							<tr><td width="135px"><span class="label">Name:</span></td><td><input type="text" name="name" value="'.$_POST['name'].'"></td></tr>
							<tr><td><span class="label">Designation:</span></td><td><span class="label">'.$faculty['designation'].'</span></td></tr>
							<tr><td><span class="label">Qualification:</span></td><td><textarea name="qualification">'.$_POST['qualification'].'</textarea></td></tr>
							<tr><td><span class="label">Office:</span></td><td><input type="text" name="office" value="'.$_POST['office'].'"></td></tr>
							<tr><td><span class="label">Email:</span></td><td><input type="text" name="email" value="'.$_POST['email'].'"></td></tr>
							<tr><td><span class="label">Phone:</span></td><td><input type="text" name="phone" value="'.$_POST['phone'].'"></td></tr>
							<tr><td></td><td><input class="button" type="submit" name="save" value="Save" /><input class="button" type="reset" name="reset" value="Reset" /></td></tr>';
					}
					else if(!$_POST['edit'] && !$_POST['save'])
					{
					echo'
							<tr><td width="135px"><span class="label">Name:</span></td><td><span class="label">'.$faculty['name'].'</span></td></tr>
							<tr><td><span class="label">Designation:</span></td><td><span class="label">'.$faculty['designation'].'</span></td></tr>
							<tr><td><span class="label">Qualification:</span></td><td><span class="label">'.$faculty['qualification'].'</span></td></tr>
							<tr><td><span class="label">Office:</span></td><td><span class="label">'.$faculty['office'].'</span></td></tr>
							<tr><td><span class="label">Email:</span></td><td><span class="label">'.$faculty['email'].'</span></td></tr>
							<tr><td><span class="label">Phone:</span></td><td><span class="label">'.$faculty['phone'].'</span></td></tr>
							<tr><td></td><td><input class="button" type="submit" name="edit" value="Edit" /></td></tr>';
					}
				?>

						</form>
					</table>
				</div><!--form-wrapper-->
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$error=NULL;
$warning=NULL;
$success=NULL;
include('php/foot.php'); ?>
