<?php
error_reporting(0);
session_start();
ob_start();

//initializations
$section='edit profile';
$page='Optional Headings';
$tablename='profiles';

require('php/functions.php');
$pagetitle='Edit Profile';

//actions

if($_POST['save'])
{ 
	$save['heading1']=mysql_real_escape_string($_POST['heading1']);
	$save['heading2']=mysql_real_escape_string($_POST['heading2']);
	$save['heading3']=mysql_real_escape_string($_POST['heading3']);
	$save['heading4']=mysql_real_escape_string($_POST['heading4']);
	$save['heading5']=mysql_real_escape_string($_POST['heading5']);
	$save['heading6']=mysql_real_escape_string($_POST['heading6']);
	$save['heading7']=mysql_real_escape_string($_POST['heading7']);
	$save['heading8']=mysql_real_escape_string($_POST['heading8']);
	$save['text1']=mysql_real_escape_string($_POST['text1']);
	$save['text2']=mysql_real_escape_string($_POST['text2']);
	$save['text3']=mysql_real_escape_string($_POST['text3']);
	$save['text4']=mysql_real_escape_string($_POST['text4']);
	$save['text5']=mysql_real_escape_string($_POST['text5']);
	$save['text6']=mysql_real_escape_string($_POST['text6']);
	$save['text7']=mysql_real_escape_string($_POST['text7']);
	$save['text8']=mysql_real_escape_string($_POST['text8']);
	
	$save_query_profiles=mysql_query("UPDATE `fes`.`".$tablename."` SET `heading1` = '".$save['heading1']."',`heading2` = '".$save['heading2']."',`heading3` = '".$save['heading3']."',`heading4` = '".$save['heading4']."',`heading5` = '".$save['heading5']."',`heading6` = '".$save['heading6']."',`heading7` = '".$save['heading7']."',`heading8` = '".$save['heading8']."', `text1` = '".$save['text1']."', `text2` = '".$save['text2']."', `text3` = '".$save['text3']."', `text4` = '".$save['text4']."', `text5` = '".$save['text5']."', `text6` = '".$save['text6']."', `text7` = '".$save['text7']."', `text8` = '".$save['text8']."' WHERE `username`='".$_SESSION['faculty username']."' LIMIT 1;"); 
	$_SESSION['success']='Changes have been saved.';
	$_POST=NULL;
	header('refresh:0'); die();
	
}

//extract entries from database
$faculty_select_query=mysql_query("SELECT * FROM `fes`.`".$tablename."` WHERE `username`='".$_SESSION['faculty username']."' LIMIT 1;");
$faculty=mysql_fetch_array($faculty_select_query);

//Assigning session messages to local message variable
$error=$_SESSION['error'];
$warning=$_SESSION['warning'];
$success=$_SESSION['success'];

$_SESSION['error']=NULL;
$_SESSION['warning']=NULL;
$_SESSION['success']=NULL;

?>
<?php include('php/head.php'); ?>
			<div id="content-wrapper">
				<h1>Optional Headings</h1>
				<div id="form-wrapper">
				<?php 
				if($error){ 
					echo'<span class="message"><b>ERROR:</b> '.$error.'</span>';}
				if($warning){ 
					echo'<span class="message"><b>WARNING:</b> '.$warning.'</span>';}
				if($success){ 
					echo'<span class="message">'.$success.'</span>';}
					
				echo'	
					<table>
						<form action="" method="post" enctype="multipart/form-data">';
					if($_POST['edit'] || $_POST['save'])
					{
					echo'
							<tr><td width="135"><span class="label">Heading 1:</span></td><td><input type="text" name="heading1" value="'.$faculty['heading1'].'"></td></tr>
							<tr><td><span class="label">Heading 1 Content:</span></td><td><textarea class="ckeditor" name="text1">'.$faculty['text1'].'</textarea></td></tr>
							<tr><td><span class="label">Heading 2:</span></td><td><input type="text" name="heading2" value="'.$faculty['heading2'].'"></td></tr>
							<tr><td><span class="label">Heading 2 Content:</span></td><td><textarea class="ckeditor" name="text2">'.$faculty['text2'].'</textarea></td></tr>
							<tr><td><span class="label">Heading 3:</span></td><td><input type="text" name="heading3" value="'.$faculty['heading3'].'"></td></tr>
							<tr><td><span class="label">Heading 3 Content:</span></td><td><textarea class="ckeditor" name="text3">'.$faculty['text3'].'</textarea></td></tr>
							<tr><td><span class="label">Heading 4:</span></td><td><input type="text" name="heading4" value="'.$faculty['heading4'].'"></td></tr>
							<tr><td><span class="label">Heading 4 Content:</span></td><td><textarea class="ckeditor" name="text4">'.$faculty['text4'].'</textarea></td></tr>
							<tr><td><span class="label">Heading 5:</span></td><td><input type="text" name="heading5" value="'.$faculty['heading5'].'"></td></tr>
							<tr><td><span class="label">Heading 5 Content:</span></td><td><textarea class="ckeditor" name="text5">'.$faculty['text5'].'</textarea></td></tr>
							<tr><td><span class="label">Heading 6:</span></td><td><input type="text" name="heading6" value="'.$faculty['heading6'].'"></td></tr>
							<tr><td><span class="label">Heading 6 Content:</span></td><td><textarea class="ckeditor" name="text6">'.$faculty['text6'].'</textarea></td></tr>
							<tr><td><span class="label">Heading 7:</span></td><td><input type="text" name="heading7" value="'.$faculty['heading7'].'"></td></tr>
							<tr><td><span class="label">Heading 7 Content:</span></td><td><textarea class="ckeditor" name="text7">'.$faculty['text7'].'</textarea></td></tr>
							<tr><td><span class="label">Heading 8:</span></td><td><input type="text" name="heading8" value="'.$faculty['heading8'].'"></td></tr>
							<tr><td><span class="label">Heading 8 Content:</span></td><td><textarea class="ckeditor" name="text8">'.$faculty['text8'].'</textarea></td></tr>
							<tr><td></td><td><input class="button" type="submit" name="save" value="Save" /><input class="button" type="reset" name="reset" value="Reset" /></td></tr>';
					}
					else if(!$_POST['edit'] && !$_POST['save'])
					{
					echo'
							<tr><td width="135"><span class="label">Heading 1:</span></td><td><span class="label">'.$faculty['heading1'].'</span></td></tr>
							<tr><td><span class="label">Heading 1 Content:</span></td><td><span class="label">'.$faculty['text1'].'</span></td></tr>
							<tr><td><span class="label">Heading 2:</span></td><td><span class="label">'.$faculty['heading2'].'</span></td></tr>
							<tr><td><span class="label">Heading 2 Content:</span></td><td><span class="label">'.$faculty['text2'].'</span></td></tr>
							<tr><td><span class="label">Heading 3:</span></td><td><span class="label">'.$faculty['heading3'].'</span></td></tr>
							<tr><td><span class="label">Heading 3 Content:</span></td><td><span class="label">'.$faculty['text3'].'</textarea></td></tr>
							<tr><td><span class="label">Heading 4:</span></td><td><span class="label">'.$faculty['heading4'].'</span></td></tr>
							<tr><td><span class="label">Heading 4 Content:</span></td><td><span class="label">'.$faculty['text4'].'</span></td></tr>
							<tr><td><span class="label">Heading 5:</span></td><td><span class="label">'.$faculty['heading5'].'</span></td></tr>
							<tr><td><span class="label">Heading 5 Content:</span></td><td><span class="label">'.$faculty['text5'].'</span></td></tr>
							<tr><td><span class="label">Heading 6:</span></td><td><span class="label">'.$faculty['heading6'].'</span></td></tr>
							<tr><td><span class="label">Heading 6 Content:</span></td><td><span class="label">'.$faculty['text6'].'</span></td></tr>
							<tr><td><span class="label">Heading 7:</span></td><td><span class="label">'.$faculty['heading7'].'</span></td></tr>
							<tr><td><span class="label">Heading 7 Content:</span></td><td><span class="label">'.$faculty['text7'].'</span></td></tr>
							<tr><td><span class="label">Heading 8:</span></td><td><span class="label">'.$faculty['heading8'].'</span></td></tr>
							<tr><td><span class="label">Heading 8 Content:</span></td><td><span class="label">'.$faculty['text8'].'</span></td></tr>
							<tr><td></td><td><input class="button" type="submit" name="edit" value="Edit" /></td></tr>';
					}
				?>

						</form>
					</table>
				</div><!--form-wrapper-->
			</div><!--content-wrapper-->
<?php 
$_POST=NULL;
$error=NULL;
$warning=NULL;
$success=NULL;
include('php/foot.php'); ?>
