	<?php echo     	'';
     switch($pageheading){
	case 'Overview':
	echo"
          	<h1>Overview</h1>
			<p>Today's Engineering problems demand solutions that require experties in a wide range of scientific deciplines. Hence, Engineering Sciences, because of its versatility, is a globally respected field. Faculty of Engineering Sciences offers a holistic perspective on the fundamental sciences, and draws on an integration of different scientific and engineering deciplines. The study of Engineerin Sciences includes a broad based coverage of multi-disciplinary engineering, and a blend of Mathematics, physics, computer sciences and engineering, which developes the skill set and the qualities in the students that are needed not only in the industry, but also in conducting research and designing innovative engineering solutions.</p>";
		   break;
		   
	case 'Mission And Objectives':
	echo"
            <h1>Mission</h1>
			<p style='font-size:17px; line-height:24px;font-style: italic;text-align:center;'>".'"'."To produce practicing engineers by providing them with solid technical preparation and 
exposure to issues and practices at leading edge of technology, and to cater for increasing demand of qualified professionals in emerging fields of technology in
Pakistan.".'"'."</p>

			<h1>Objectives</h1>
			<ul class='pbullet'>
			<li class='pbullet'>To prepare students to design engineering multidisciplinary systems and processes.</li>
			<li class='pbullet'>To prepare students to use modern computational and experimental equipment used in both industry and research organizations.</li>
			<li class='pbullet'>To develop in each student the broad background required to play active role equally in industry and R&D work.</li>
			<li class='pbullet'>To promote development of communication skills and individual professionalism through presentations in experimental methods, design, and technical writing courses.</li>
			<li class='pbullet'>To prepare students to operate in a global environment.</li>
			</ul>";
		   break;
		
		   
	case "Dean's Message":
	echo"
            <h1>Dean's Message</h1>
			<p>Engineering Sciences program is offered in most of the leading universities of the world including Harvard, Cornell, and UC Berkeley. In pursuit of excellence GIKI pioneered in introducing ES program in Pakistan.</p>
			<p>The ES program will grow in order to cater the R&D demands of our country. In future we plan to establish Research Center of Modeling and Simulation and center of Photonics we also plan to offer degree courses in Applied Mathematics and Applied Physics program.</p>
			<p>I would strongly encourage the youth of Pakistan to choose this contemporary engineering field for their future career.</p>
			<p style='float:right'><b>Prof. Dr. Jameel-Un Nabi</b><br>Dean, FES, GIK Institute</p>
			";
			break;
	
	case 'What is Engineering Sciences?':
	echo"
            <h1>What is Engineering Sciences?</h1>
			<p>Engineering Science is a multidisciplinary program that emphasizes enhanced understanding and integrated application of engineering, scientific, and mathematical principles. </p>
			<p>The program is unique because it provides a broad foundation in the sciences and associated mathematics that underlie engineering and provides students the opportunity to obtain a depth of knowledge in an area of their choice through technical electives. The curriculum is designed for students who seek to link the engineering disciplines with science.</p>
			<p>Focus areas of study include, but are not limited to, electrical, mechanical, civil, bioengineering, and materials and are typically interdisciplinary. Hence, Engineering Science students achieve both depth and breadth in engineering and science, are able to function across disciplines, and graduate well prepared for advanced studies as well as professional employment.</p>
			";
		   break;
		   
}
?>